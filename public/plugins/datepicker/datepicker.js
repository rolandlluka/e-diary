$(document).ready(function () {
    $('#datepicker2').datepicker({
        autoclose:true,
        locale: 'sq',
        format: 'yyyy-mm-dd',
        orientation: 'bottom auto'
    });
    $('#datepicker').datepicker({
        autoclose:true,
        locale: 'sq',
        format: 'yyyy-mm-dd',
        orientation: 'bottom auto'
    });

   
});
$(function () {
    $('#datetimepicker4').datetimepicker({
    	stepping: 15,
        format: 'YYYY-MM-DD HH:mm'
    });
 });