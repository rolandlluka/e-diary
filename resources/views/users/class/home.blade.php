@extends('layouts.app')

@section('stylesheets')
    <style>body {
            background: #ffffff;
        }</style>
@stop


@section('content')

  <section class="content-header">
      <h1>
       Klasat
      </h1>
  @include('partials.message-block')
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Ballina</a></li>
        <li><a href="{{route('homeClass')}}">Klasat</a></li>
      </ol>
  </section>

<!-- Main content -->
<section class="content">

<!-- Default box -->
<div class="box">
    <div class="box-header with-border">
        <a href="{{route('createClass')}}" class="btn btn-primary">Shto Klasat</a>
    </div>
    <div class="box-body">
        <div class="box-body table-responsive no-padding">
            <table class="table table-responsive table-hover">
                <thead>
                <tr>
                <th>Numri rendor</th>
                <th>Klasa/Paralelja</th>
                <th>Kujdestari</th>
                <th>Statusi</th>
                <th>Edito</th>
                </tr>
                </thead>
        @foreach($class as $clas)
        <tr>
            <td>
            {{ $clas->id}}
            </td>
            <td>
                {{ $clas->class}} / {{ $clas->parallel}}
            </td>
            {{--{{$clas->class_administrator}}--}}
            <td>@foreach($clas->professor as $egzon)
                <span class="badge">{{ $egzon->name }}</span>
                @endforeach
            </td>

            <td>
            @if($clas->status == false)
                <form action="{{route('activeClass', $clas->id)}}" method="POST">
                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                <button type="submit" class="btn btn-xs btn-success"
                    onclick="return confirm('A jeni te sigurte qe deshironi te aktivizoni Klasen?');">
                    Aktivizo
                </button>
                </form>
                @else
                <form action="{{route('passiveClass', $clas->id)}}" method="POST">
                    <input type="hidden" name="_token" value="{{csrf_token() }}"/>
                    <button type="submit" class="btn btn-xs btn-danger"
                        onclick="return confirm('A jeni te sigurte qe deshironi te pasivizoni Klasen?');">
                        Pasivizo
                    </button>

                {{--<a class="glyphicon glyphicon-remove" role="button" type="submit">Delete</a>--}}
                </form>

            </td>
            @endif
            <td>
            <form action="{{route('deleteClass', $clas->id)}}" method="POST">
                <input type="hidden" name="_token" value="{{csrf_token() }}"/>
                    <a class="glyphicon glyphicon-pencil"
                     href="{{route('editClass', $clas->id)}}">Edit </a>
                <button type="submit" class="btn btn-xs btn-danger"
                    onclick="return confirm('A jeni të sigurtë qe doni ta fshini?');">
                    Delete
                </button>
            </form>
            </td>
        </tr>
        @endforeach
        </table>
        {!! $class->render() !!}
        </div>
    </div>
</div>
</section>
@endsection

