@extends('layouts.app')
@section('content')
<section class="content-header">
      <h1>
        Shto perdorues
      </h1>
      <ol class="breadcrumb">
        @include('partials.message-block')
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
     
        <!-- text input -->
        <div class="form-group">
        <div class="box-header with-border">
          
        </div>
        <div class="box-body">
         <form action="{{route('storeClass')}}" method="POST">
         <div class="form-group">
            <label>Klasa</label>
              <input class="form-control" type="text" name="class"/>
              <input type="hidden" name="_token" value="{!! csrf_token() !!}">
         </div>
          <div class="form-group">
            <label>Paralelja</label>
              <input class="form-control" type="text" name="parallel"/>
         </div>
         <div class="form-group">
            <label>Kujdestari</label>
             <select class="form-control" name="class_administrator">
                <option value="0">Zgjedh nga lista</option>
                @foreach($professors as $professor)
                    <option value="{{$professor->id}}">{{$professor->name}}  {{$professor->lastname}}</option>
                @endforeach
            </select>
         </div>   
         <button class="btn btn-primary" type="submit">Regjistro</button>
         <a href="{{route('homeClass')}}" class="btn btn-primary">Lista e Klasave</a>
    </form>
</div>
        </div>
</div>
</section>
@endsection