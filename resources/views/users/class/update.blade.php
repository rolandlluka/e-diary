@extends('layouts.app')

@section('content')
<section class="content-header">
      <h1>
        Klasat
      </h1>
      <ol class="breadcrumb">
        @include('partials.message-block')
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
     
        <!-- text input -->
        <div class="form-group">
        <div class="box-header with-border">
          <h3 class="box-title">Title</h3>
        </div>
        <div class="box-body">
         <form method="post" action="{{route('updateClass', $class->id)}}">
         <input type="hidden" name="_token" value="{!! csrf_token() !!}">
             <div class="form-group">
                <label>Klasa</label>
                  <input class="form-control" type="text" name="class" value="{{$class->class}}"/>
             </div>
              <div class="form-group">
                <label>Paralelja</label>
                  <input class="form-control" type="text" name="parallel" value="{{$class->parallel}}"/>
             </div>
              <div class="form-group">
            <label>Kujdestari</label>
                 <select class="form-control" name="class_administrator">
                    <option value="0">Zgjedh nga lista</option>
                    @foreach($professors as $professor)
                        <option value="{{$professor->id}}">{{$professor->name}}</option>
                    @endforeach
                </select>
             </div> 
             <button class="btn btn-primary" type="submit">Ruaje</button>
             <a href="{{route('homeClass')}}" class="btn btn-primary">Lista e Klasave</a>
            </form>
        </div>
    </div>
</div>
</section>
@endsection

