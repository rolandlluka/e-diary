@extends('layouts.app')

@section('stylesheets')
    <style>body {
            background: #ffffff;
        }</style>
@stop

@section('content')
@include('partials.message-block')
<section class="content-header">
      <h1>
       Perdoruesit
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Ballina</a></li>
        
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <a href="{{route('createParents')}}" class="btn btn-primary">Shto Prinderit</a>
        </div>
        <div class="box-body">
          <div class="box-body table-responsive no-padding">
              <table class="table table-hover">

                <thead>
                    <tr>
                       <th>Ermi dhe Mbiemri</th>
                        <th>Gjinia</th>
                        <th>Datelindja</th>
                        <th>Vendlindja</th>
                        <th>Statusi</th>
                        <th>Edito</th>
                     </tr>
                </thead>
                    <tbody>

                         @foreach($parents as $parent)
                                    <tr>
                                        <td>
                                            {{ $parent->name}} {{ $parent->lastname}} 
                                        </td>
                                        @if($parent->gender == 'M')
                                            <td>Mashkull</td>
                                        @else
                                            <td>Femer</td>
                                        @endif
                                        <td>
                                            {{ $parent->birthday}}
                                        </td>
                                        <td>
                                            {{ $parent->birthplace}}
                                        </td>
                                        <td>
                                            @if($parent->status == false)
                                                <form action="{{route('activeParents', $parent->id)}}" method="POST">
                                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                                    <button type="submit" class="btn btn-xs btn-success"
                                                            onclick="return confirm('A jeni te sigurte qe deshironi te aktivizoni Prinderin?');">
                                                        Aktivizo
                                                    </button>
                                                </form>
                                            @else
                                                <form action="{{route('passiveParents', $parent->id)}}" method="POST">
                                                    <input type="hidden" name="_token" value="{{csrf_token() }}"/>
                                                    <button type="submit" class="btn btn-xs btn-danger"
                                                            onclick="return confirm('A jeni te sigurte qe deshironi te pasivizoni Prinderin?');">
                                                        Pasivizo
                                                    </button>

                                                    {{--<a class="glyphicon glyphicon-remove" role="button" type="submit">Delete</a>--}}
                                                </form>

                                        </td>
                                        @endif
                                        <td>
                                        <form action="{{route('deleteParents', $parent->id)}}" method="POST">
                                            <input type="hidden" name="_token" value="{{csrf_token() }}"/>
                                            <a class="glyphicon glyphicon-pencil"
                                               href="{{route('editParents', $parent->id)}}">Edit </a>
                                            <button type="submit" class="btn btn-xs btn-danger"
                                            onclick="return confirm('A jeni të sigurtë qe doni ta fshini?');">
                                            Delete
                                            </button>
                                        </form>
                                        </td>
                                    </tr>
                                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
</section>
@stop

