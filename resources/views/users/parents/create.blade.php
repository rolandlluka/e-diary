@extends('layouts.app')
@section('content')

<section class="content-header">
      <h1>
        Shto perdorues
      </h1>
      @include('partials.message-block')
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
     
        <!-- text input -->
        <div class="form-group">
        <div class="box-header with-border">
          
        </div>
        <div class="box-body">
         <form action="{{route('storeParents')}}" method="POST">
         <input type="hidden" name="_token" value="{!! csrf_token() !!}">
         <div class="form-group">
            <label>Emri</label>
              <input class="form-control" type="text" name="name"/>
         </div>
         <div class="form-group">
            <label>Mbiemri</label>
                <input class="form-control" type="text" name="lastname"/>
         </div>
         <div class="form-group">
            <label>Email</label>
              <input class="form-control" type="text" name="email" />
         </div>
         <div class="form-group">
            <label>Password</label>
              <input class="form-control" type="password" name="password" />
         </div>
          <div class="form-group">
            <label>Gjinia</label>
              <input type="radio" value="M" name="gender"/>M
             <input type="radio" value="F" name="gender" style="margin-left: 10px"/>F
         </div>
         <div class="form-group">
            <label>Ditelindja</label>
              <input class="form-control" id="datepicker" name="birthday">
         </div>
         <div class="form-group">
            <label>Vendlinja</label>
              <input class="form-control" type="text" name="birthplace">
         </div>
         <button class="btn btn-primary" type="submit">Regjistro</button>
         <a href="{{route('homeParents')}}" class="btn btn-primary">Lista e Prinderve</a>
    </form>
</div>
        </div>
</div>
</section>
@endsection

