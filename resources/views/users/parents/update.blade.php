@extends('layouts.app')

@section('content')
<section class="content-header">
      <h1>
        Perdoruesit
      </h1>
      @include('partials.message-block')
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
     
        <!-- text input -->
        <div class="form-group">
        <div class="box-header with-border">
          <h3 class="box-title">Title</h3>
        </div>
        <div class="box-body">
         <form action="{{route('updateParents', $parents->id)}}" method="POST">
         <input type="hidden" name="_token" value="{{csrf_token()}}"/>
         <div class="form-group">
            <label>Emri</label>
              <input class="form-control" type="text" name="name" value="{{$parents->name}}"/>
              <input type="hidden" name="_token" value="{!! csrf_token() !!}">
         </div>
         <div class="form-group">
            <label>Mbiemri</label>
              <input class="form-control" type="text" name="lastname" value="{{$parents->lastname}}"/>
              <input type="hidden" name="_token" value="{!! csrf_token() !!}">
         </div>
          <div class="form-group">
            <label>Gjinia</label>
              <input type="radio" value="M" name="gender"
                     @if($parents->gender == 'M') checked @endif>M
            <input type="radio" value="F" name="gender" style="margin-left: 10px;"
                     @if($parents->gender == 'F') checked @endif>F
         </div>
         <div class="form-group">
            <label>Ditelindja</label>
              <input class="form-control" id="datepicker" name="birthday" value="{{$parents->birthday}}"/>             
         </div> 
         <div class="form-group">
            <label>Vendlindja</label>
              <input class="form-control" type="text" name="birthplace" value="{{$parents->birthplace}}">          
         </div> 
            <button class="btn btn-primary" type="submit">Ruaje</button>
            <a href="{{route('homeParents')}}" class="btn btn-primary">Lista e Prinderve</a>
        </form> 
</div>
        </div>
</div>
</section>
@endsection
