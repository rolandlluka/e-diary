@extends('layouts.app')
@section('content')

<section class="content-header">
      <h1>
        Programi mesimor per profesor
      </h1>
      <ol class="breadcrumb">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
 <div class="box">
      <div class="box-body">
    
      <table class="table table-bordered table-hover" id="tab_logic">
      <thead>
           <tr>
              <th>Klasa</th>
               <th>Profesori</th>
               <th>Nga:</th>
               <th>Deri me:</th>
            </tr>
       </thead>
        <tbody>
          <tr >
            <td>
            <select name="class" id="class" class="form-control">
              <option value="" disabled selected></option>
              @foreach($class as $cl)
                <option value="{{$cl->id}}"> {{$cl->class}}/{{$cl->parallel}} </option>
              @endforeach
            </select>
            </td>
            <td>
            <select name="professor" id="professor" class="form-control">
              <option value="" disabled selected>Profesori</option>
             @foreach($professor as $prof)
                <option value="{{$prof->id}}">{{$prof->name}} {{$prof->lastname}}</option>
              @endforeach
           </select>
            </td>
            <td>
            <div class="form-group">
              <input type='text'  class="form-control" id='datepicker' name="nga">
            </div>
           </td>
           <td>
            <div class="form-group">
              <input type='text'  class="form-control" id='datepicker2' name="deri">
            </div>
           </td>
           <td>
            <div class="form-group">
              <a class="btn btn-success" onclick="test()">Filtro </a>
            </div>
           </td>
          </tr>
        </tbody>
      </table>
      <div class="box-body table-responsive no-padding">
    <div class="tabela"> </div>
    </div>
    </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">
function test(){

var clas = $('#class').val();
var professor = $('#professor').val();
var data1 = $('#datepicker').val();
var data2 = $('#datepicker2').val();

$.ajax({
    type:'GET',
    url : "programi-ajax/"+clas+"/"+professor+"/"+data1+"/"+data2+"",
    success:function(datas){
     
    var tabela = "<table class='table table-bordered'><thead><tr><td style='font-weight:bold;'>Klasa</td><td style='font-weight:bold;'>Profesori</td><td style='font-weight:bold;'>Data</td><td style='font-weight:bold;'>Pershkrimi</td></tr></thead>";
    
     datas.forEach(function(entry) {
         tabela += "<tbody><tr><td style='width:500px;'>"+entry.clas.class+"/"+entry.clas.parallel+"</td><td style='width:500px;'>"+entry.professor.name+" "+entry.professor.lastname+"</td><td style='width:500px;'>"+entry.ora+"</td><td style='width:500px;'>"+entry.pershkrimi+"</td></tr></tbody>"
     });

     tabela += "</table>";

     $('.tabela').html(tabela);



     }
   });

}
</script>
@endsection

