@extends('layouts.app')

@section('content')
@include('partials.message-block')
<section class="content-header">
      <h1>
       Perdoruesit
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Ballina</a></li>
        
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <a href="{{route('createProfessor')}}" class="btn btn-primary">Shto Profesor</a>
        </div>
        <div class="box-body">
          <div class="box-body table-responsive no-padding">
              <table class="table table-hover">

                <thead>
                    <tr>
                       <th>Emri</th>
                        <th>Mbiemri</th>
                        <th>Gjinia</th>
                        <th>Ditelindja</th>
                        <th>Lenda</th>
                        <th>Aktiv/Pasiv</th>
                        <th>Edito</th>
                     </tr>
                </thead>
                    <tbody>
                    @foreach($professors as $prof)
                                <tr>
                                    <td>
                                        {{$prof->name}}
                                    </td>
                                    <td>{{$prof->lastname}}</td>
                                    @if($prof->gender == 'M')
                                        <td>Mashkull</td>
                                    @else
                                        <td>Femer</td>
                                    @endif
                                    <td>{{$prof->birthday}}</td>
                                    <td>
                                        @foreach($prof->subjects as $subject)
                                            <span class="badge">{{ $subject->name }}</span>
                                        @endforeach
                                    </td>
                                    <td>
                                        @if($prof->status == false)
                                        <form action="{{route('activeProfessor', $prof->id)}}" method="POST">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}" />
                                        <button type="submit" class="btn btn-xs btn-success"
                                                onclick="return confirm('A jeni te sigurte qe deshironi te aktivizoni Profesorin ?');">
                                            Aktivizo
                                        </button>
                                        </form>
                                        @else
                                            <form action="{{route('passiveProfessor', $prof->id)}}" method="POST">
                                                <input type="hidden" name="_token" value="{{csrf_token() }}"/>
                                                <button type="submit" class="btn btn-xs btn-danger"
                                                        onclick="return confirm('A jeni te sigurte qe deshironi te pasivizoni Profesorin ?');">
                                                    Pasivizo
                                                </button>
                                                {{--<a class="glyphicon glyphicon-remove"  role="button" type="submit">Delete</a>--}}
                                            </form>

                                        @endif
                                    </td>
                                    <td>
                                           <form action="{{route('deleteProfessors', $prof->id)}}" method="POST">
                                            <input type="hidden" name="_token" value="{{csrf_token() }}"/>
                                            <a class="glyphicon glyphicon-pencil"
                                               href="{{route('editProfessor', $prof->id)}}">Edit </a>
                                            <button type="submit" class="btn btn-xs btn-danger"
                                            onclick=" return confirm('A jeni të sigurtë?');">
                                            Delete
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                        @endforeach
                </tbody>
            </table>
             {!! $professors->render() !!}
        </div>
    </div>
</div>
</section>
@endsection
