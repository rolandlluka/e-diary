@extends('layouts.app')

@section('stylesheets')
   
@stop

@section('content')

<section class="content">
        <div class="form-group">
        <div class="box-header with-border">
          <h3 class="box-title">Title</h3>
        </div>
        <div class="box-body">
         <form action="{{route('updateProfessor', $professor->id)}}" method="POST">
         <div class="form-group">
            <label>Emri</label>
              <input class="form-control" type="text" name="name" value="{{$professor->name}}" />
              <input type="hidden" name="_token" value="{!! csrf_token() !!}">
         </div>
          <div class="form-group">
            <label>Mbiemri</label>
              <input class="form-control" type="text" name="lastname" value="{{$professor->lastname}}" />
         </div>
         <div class="form-group">
            <label>Datelindja</label>
              <input class="form-control" id="datepicker" name="birthday" value="{{$professor->birthday}}">           
         </div> 
         <div class="form-group">
            <label>Gjinia</label>
            M <input type="radio" value="M" name="gender" @if($professor->gender == 'M') checked @endif>
            F <input type="radio" value="F" name="gender" @if($professor->gender == 'F') checked @endif>       
         </div>
         <div class="form-group">
            <label>Lenda</label>
              @foreach($subjects as $sub)
                <div class="form-group">
                    <input type="checkbox" name="subject[{{ $sub->id }}]"  @if(in_array($sub->id, $hasSubjects)) checked @endif> {{ $sub->name }}
                </div>
            @endforeach      
         </div> 
         <form action="{{route('updateProfessor', $professor->id)}}" method="POST">
            <input type="hidden" name="_token" value="{{csrf_token()}}" />
            <button class="btn btn-primary" type="submit">Ruaj</button>
            <a href="{{route('homeProfessor')}}" class="btn btn-primary">Lista e Profesorve</a>
        </form>
    </form>
</div>
        </div>
</div>
</section>
@endsection

@section('scripts')

@endsection

