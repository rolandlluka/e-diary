@extends('layouts.app')
@section('content')

<section class="content-header">
      <h1>
        Shto profesor
      </h1>
      <ol class="breadcrumb">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
     
        <!-- text input -->
        <div class="form-group">
        <div class="box-header with-border">
          
        </div>
        <div class="box-body">
         <form action="{{route('storeProfessor')}}" method="POST">
         <div class="form-group">
            <label>Emri</label>
           <input class="form-control" type="text" name="name"/>
           <input type="hidden" name="_token" value="{!! csrf_token() !!}">
         </div>
          <div class="form-group">
            <label>Mbiemri</label>
                <input class="form-control" type="text" name="lastname"/>
         </div>
          <div class="form-group">
            <label>Email</label>
              <input class="form-control" type="text" name="email" />
         </div>
         <div class="form-group">
            <label>Password</label>
              <input class="form-control" type="password" name="password" />
         </div>
         <div class="form-group">
            <label>Ditelindja</label>
              <input class="form-control" id="datepicker" name="birthday">
         </div>
         <div class="form-group">
            <label>Vendlindja</label>
              <input class="form-control" type="text" name="birthplace">
         </div>
         <div class="form-group">
            <label>Gjinia</label>
              <input type="radio" value="M" name="gender"/> M
              <input type="radio" value="F" name="gender" style="margin-left: 10px;"/> F
         </div>
         <div class="form-group">
            <label>Lenda</label>
              @foreach($subjects as $sub)
                <div class="form-group">
                    <input type="checkbox" name="subject[{{ $sub->id }}]"> {{ $sub->name }}
                </div>
            @endforeach
         </div>
         <button class="btn btn-primary" type="submit">Regjistro</button>
         <a href="{{route('homeProfessor')}}" class="btn btn-primary">Lista e Profesoreve</a>
    </form>
   </div>
 </div>
</div>
</section>
@endsection

