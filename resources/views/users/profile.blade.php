@extends('layouts.app')

@section('content')
<section class="content">
@include('partials.message-block')
<div class="row">
        
       <div class="col-md-12">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#activity" data-toggle="tab">Info</a></li>
              <li><a href="#settings" data-toggle="tab">Parametrat</a></li>
            </ul>
            <div class="tab-content">
            
              <div class="active tab-pane" id="activity">
              <form  class="form-horizontal" action="{{route('uploadAvatar', Auth::user()->id)}}" method="POST" enctype="multipart/form-data">
              <input type="hidden" name="_token" value="{!! csrf_token() !!}">
              
                <img class="profile-user-img img-responsive img-circle" src="{{URL::asset('/dist/avatars/')}}/{{$user->avatar}}" alt="User profile picture">
                <div class="input-group" style="text-align: center;width: 100px;margin: 0 auto;">
                <input type="file" name="avatar" id="avatar">
                 <span class="input-group-btn" style="width:0;">
                 <button type="submit" class="btn btn-default">Ndrysho</button>
                 </span>
                 </div>
              </form>
              <h3 class="profile-username text-center">{{$user->name}} {{$user->lastname}}</h3>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Emri</b> <a class="pull-right">{{$user->name}} {{$user->lastname}}</a>
                </li>
                <li class="list-group-item">
                  <b>Roli ne sistem</b> <a class="pull-right">
                      @if($user->role_id == 1)
                        Administrator
                      @elseif($user->role_id == 2)
                      Nxenes
                      @elseif($user->role_id == 3)
                      Profesor
                      @else
                      Prind
                      @endif
                  </a>
                </li>
              </ul>
              <a href="#settings" class="btn btn-primary" data-toggle="tab" ><b>Ndrysho</b></a>
              </div>

              <div class="tab-pane" id="settings">
                <form id="login_form" class="form-horizontal" action="{{route('changeProfile', $user->id)}}"  method="POST">
                  <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Emri Mbiemri</label>

                    <div class="col-sm-10">
                      <input type="email" class="form-control" id="inputName" disabled="" value="{{$user->name }} {{$user->lastname}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-2 control-label">Email</label>

                    <div class="col-sm-10">
                      <input type="email" class="form-control" id="inputEmail" disabled="" value="{{$user->email }}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputName" maxlength="2" class="col-sm-2 control-label">Passwordi i vjeter</label>

                    <div class="col-sm-10">
                      <input type="password" class="form-control" name="oldpassword" id="oldpassword" placeholder="Passwordi i vjeter">
                    </div>
                  </div>
                  <div class="form-group">
                    <label  for="inputName" class="col-sm-2 control-label">Passwordi i ri</label>

                    <div class="col-sm-10">
                      <input type="password" class="form-control" name="newpassword" id="newpassword" placeholder="Passwordi i ri">
                    </div>
                  </div>
                  <div class="form-group">
                    <label  for="inputName" class="col-sm-2 control-label">Konfirmo Passwordin</label>

                    <div class="col-sm-10">
                      <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Konfirmo passwordin e ri">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-danger">Ndrysho</button>
                    </div>
                  </div>
                </form>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      </section>
@endsection
@section('scripts')


<script type="text/javascript">
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });

    $('#login_form').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            
            oldpassword: {
                validators: {
                    notEmpty: {
                        message: 'Passwordi juaj nuk eshte valid'
                    },
                    password: {
                        message: 'Passwordi juaj nuk eshte valid'
                    },
                    stringLength: {
                        message: 'Passwordi juaj kishte se paku 6 karaktere',
                        min: function (value, validator, $field) {
                            return 6 - (value.match(/\r/g) || []).length;
                        }
                    }
                }
            },
            newpassword: {
                validators: {
                    notEmpty: {
                        message: 'Passwordi juaj nuk eshte valid'
                    },
                    password: {
                        message: 'Passwordi juaj nuk eshte valid'
                    },
                    stringLength: {
                        message: 'Passwordi juaj duhet te jete se paku 6 karaktere',
                        min: function (value, validator, $field) {
                            return 6 - (value.match(/\r/g) || []).length;
                        }
                    }
                }
            },
            password_confirmation: {
                validators: {
                    notEmpty: {
                        message: 'Passwordi juaj nuk eshte valid'
                    },
                    password: {
                        message: 'Passwordi juaj nuk eshte valid'
                    },
                    stringLength: {
                        message: 'Passwordi juaj duhet te jete se paku 6 karaktere',
                        min: function (value, validator, $field) {
                            return 6 - (value.match(/\r/g) || []).length;
                        }
                    }
                }
            }
            }
        })
        
  });

</script>



@endsection