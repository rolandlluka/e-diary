@extends('layouts.app')

@section('content')
<section class="content-header">
 @include('partials.message-block')
      <h1>
        Shto orarin e ri
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
     
        <!-- text input -->
        <div class="form-group">
        <div class="box-header with-border">
          
        </div>
        <div class="box-body">
         <form action="{{route('storeSchedule')}}" method="POST" class="form-horizontal">
         <input type="hidden" name="_token" value="{!! csrf_token() !!}">
         <div class="form-group">
            <label>Klasa</label>
              <select name="clas" id="clas" class="selectpicker form-control" data-show-subtext="true" data-live-search="true">
              <option value="0" disabled selected>Klasa</option>
             @foreach($class as $clas)
                <option value="{{$clas->id}}">{{$clas->class}}/{{$clas->parallel}}</option>
              @endforeach
              </select>
         </div>
          <div class="form-group">
            <label>Lenda</label>
              <select name="subject" id="subject" class="selectpicker form-control" data-show-subtext="true" data-live-search="true">
              <option value="0" disabled selected>Lenda</option>
             @foreach($subjects as $sub)
                <option value="{{$sub->id}}">{{$sub->name}}</option>
              @endforeach
              </select>
         </div>
         <div class="form-group">
            <label>Profesori</label>
            <select name="professor" id="professor" class="form-control">
              <option value=""></option>
            </select>
         </div>
         <div class="form-group">
            <select name="dita" id="dita" class="selectpicker form-control" data-show-subtext="true" data-live-search="false">
              <option value="0" disabled selected>Dita</option>
             
                <option value="1">E Hëne</option>
                <option value="2">E Marte</option>
                <option value="3">E Mërkure</option>
                <option value="4">E Enjte</option>
                <option value="5">E Premte</option>
              </select>
         </div>
         <div class="form-group">
            <label >Koha e fillimit</label>
              <input class="form-control" type="text" name="koha_fillimit" id="koha_fillimit" />
         </div> 
         <div class="form-group">
            <label>Koha e mbarimit</label>
              <input class="form-control" type="text" name="koha_mbarimit" id="koha_mbarimit"/>
         </div> 
         <button class="btn btn-primary" type="submit">Ruaj</button>
         <a href="{{route('homeSchedules')}}" class="btn btn-primary">Kthehu Prapa</a>
    </form>
</div>
</div>
</div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">
    $('#koha_fillimit').timepicker();
    $('#koha_mbarimit').timepicker();

$('#subject').on('change', function(e){
        console.log(e);

        var subject_id = e.target.value;

        $.get('ajax-professor?subject_id=' +subject_id, function(data){

           $('#professor').empty();

            $.each(data, function(index, studObj){

              $('#professor').append('<option value="'+studObj.professor_id+'">'+studObj.professor.name+' '+studObj.professor.lastname+'</option>')
            });
        });

      });
</script>

@endsection