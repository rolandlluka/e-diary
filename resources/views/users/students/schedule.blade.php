@extends('layouts.app')

@section('content')
<section class="content-header">
 @include('partials.message-block')
      <h1>
       Orari
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Ballina</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <a href="{{route('createSchedules')}}" class="btn btn-primary">Shto Orarin</a>
        </div>
        <div class="box-body">
          <div class="box-body table-responsive no-padding">
              <table class="table table-hover">

                <thead>
                    <tr>
                       <th>Klasa/Paralelja</th>
                        <th>Lenda</th>
                        <th>Profesori</th>
                        <th>Dita</th>
                        <th>Koha e fillimit</th>
                        <th>Koha e mbarimit</th>
                        <th></th>
                     </tr>
                </thead>
                    <tbody>
                        @foreach($schedules as $schedule)
                        <tr>
                            <td>
                                {{$schedule->clas->class}}/{{$schedule->clas->parallel}}
                            </td>
                          
                            <td>{{$schedule->subject->name}}</td>
                            <td>{{$schedule->professor->name}} {{$schedule->professor->lastname}}</td>
                            <td>
                            @if($schedule->dita == 1 )
                             E hene
                             @elseif($schedule->dita  == 2)
                             E marte
                             @elseif($schedule->dita == 3)
                              E merkure
                              @elseif($schedule->dita  == 4)
                              E enjte
                              @else($schedule->dita  == 5)
                              E premte
                              @endif

                            </td>
                            <td>{{$schedule->koha_fillimit}}</td>
                            <td>{{$schedule->koha_mbarimit}}</td>
                            <td>
                            <form action="{{route('deleteSchedules', $schedule->id)}}" method="POST">
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                                <a class="glyphicon glyphicon-pencil"
                                   href="{{route('editSchedules', $schedule->id)}}">Edit </a>
                                <button type="submit" class="btn btn-xs btn-danger" onclick="return confirm('Are you sure you want to delete this tem?')">Delete</button>   
                            </form>  
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
</section>
@endsection







