@extends('layouts.app')

@section('content')
<section class="content-header">
 @include('partials.message-block')
      <h1>
        Shto nxenes
      </h1>
     
 
    

    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
     
        <!-- text input -->
        <div class="form-group">
        <div class="box-header with-border">
          
        </div>
        <div class="box-body">
         <form action="{{route('storeStudents')}}" method="POST">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
         <div class="form-group">
            <label>Emri</label>
             <input class="form-control" type="text" name="name" />
         </div>
         <div class="form-group">
            <label>Mbiemri</label>
             <input class="form-control" type="text" name="lastname" />
         </div>
         <div class="form-group">
            <label>Email</label>
              <input class="form-control" type="text" name="email" />
         </div>
         <div class="form-group">
            <label>Password</label>
              <input class="form-control" type="password" name="password" />
         </div>
          <div class="form-group">
            <label>Prindi</label>
              <select name="parent" id="parent" class="selectpicker form-control" data-show-subtext="true" data-live-search="true">
              <option value="" disabled selected>Prindi</option>
             @foreach($parents as $parent)
                <option value="{{$parent->id}}">{{$parent->name}} {{$parent->lastname}}</option>
              @endforeach
              </select>
         </div>
         <div class="form-group">
            <label>Viti Akademik</label>
              <select name="academic" id="academic" class="selectpicker form-control" data-show-subtext="true" data-live-search="true">
              <option value="" disabled selected>Viti</option>
             @foreach($academic as $acad)
                <option value="{{$acad->id}}">{{$acad->year}}</option>
              @endforeach
              </select>
         </div>
         <div class="form-group">
            <label>Klasa</label>
              <select name="clas" id="clas" class="selectpicker form-control" data-show-subtext="true" data-live-search="true">
              <option value="" disabled selected>Viti</option>
             @foreach($clas as $clas)
                <option value="{{$clas->id}}">{{$clas->class}} / {{$clas->parallel}}</option>
              @endforeach
              </select>
         </div>
         <div class="form-group">
            <label>Vendlindja</label>
              <input class="form-control" type="text" name="birthplace">
         </div>
         <div class="form-group">
            <label>Vendbanimi</label>
              <input class="form-control" type="text" name="place">
         </div>
         <div class="form-group">
            <label>Datelindja</label>
              <input class="form-control" id="datepicker" name="birthday"/>
         </div>
         <div class="form-group">
             <input type="radio" value="M" name="gender" />M
             <input type="radio" value="F" name="gender" style="margin-left: 10px;"/>F
         </div>
         <button class="btn btn-primary" type="submit">Regjistro</button>
         <a href="{{route('homeStudents')}}" class="btn btn-primary">Lista e Nxenesve</a>
    </form>
   </div>
 </div>
</div>
</section>
@endsection

