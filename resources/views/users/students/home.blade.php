@extends('layouts.app')

@section('content')

<section class="content-header">
@include('partials.message-block')
      <h1>
       Nxenesit
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Ballina</a></li>
      </ol>
</section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <a href="{{route('createStudents')}}" class="btn btn-primary">Shto Nxenes</a>
        </div>
        <div class="box-body">
          <div class="box-body table-responsive no-padding">
              <table class="table table-hover">

                <thead>
                    <tr>
                       <th>Emri & Mbiemri</th>
                        <th>Emri Prindit</th>
                        <th>Viti Akademik</th>
                        <th>Klasa</th>
                        <th>Vendlindja</th>
                        <th>Vendbanimi</th>
                        <th>Datelindja</th>
                        <th>Gjinia</th>
                        <th>Aktiv/Pasiv</th>
                        <th>Action</th>
                     </tr>
                </thead>
                    <tbody>
                            @foreach($students as $student)
                                    <tr>
                                        <td>
                                            {{$student->name}} {{$student->lastname}}
                                        </td>
                                        <td>
                                            {{$student->parent->name}} {{$student->parent->lastname}}

                                        </td>
                                        <td>
                                            {{$student->academic->year}}
                                        </td>
                                        <td>
                                            {{$student->clas->class}}/{{$student->clas->parallel}}
                                        </td>
                                        <td>{{$student->birthplace}}</td>
                                        <td>{{$student->place}}</td>
                                        <td>{{$student->birthday}}</td>
                                        @if($student->gender == '0')
                                            <td>Mashkull</td>
                                        @else
                                            <td>Femer</td>
                                        @endif

                                        <td>
                                            @if($student->status == false)
                                                <form action="{{route('activeStudents', $student->id)}}" method="POST">
                                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                                    <button type="submit" class="btn btn-xs btn-danger"
                                                            onclick="return confirm('A jeni te sigurte qe deshironi te aktivizoni Nxenesin ?');">
                                                        Aktivizo
                                                    </button>
                                                </form>
                                            @else
                                                <form action="{{route('passiveStudents', $student->id)}}"
                                                      method="POST">
                                                    <input type="hidden" name="_token" value="{{csrf_token() }}"/>
                                                    <button type="submit" class="btn btn-xs btn-success"
                                                            onclick="return confirm('A jeni te sigurte qe deshironi te pasivizoni Nxenesin ?');">
                                                        Pasivizo
                                                    </button>
                                                    {{--<a class="glyphicon glyphicon-remove" role="button" type="submit">Delete</a>--}}
                                                </form>

                                            @endif
                                        </td>
                                        <td>
                                        <form action="#" method="POST">
                                            <input type="hidden" name="_token" value="{{csrf_token() }}"/>
                                            <a class="glyphicon glyphicon-pencil"
                                               href="{{route('editStudents', $student->id)}}">Edit </a>
                                            <button type="submit" class="btn btn-xs btn-danger"
                                            onclick=" return confirm('A jeni të sigurtë?');">
                                            Delete
                                            </button>
                                        </form>
                                        </td>
                                    </tr>
                            @endforeach
                </tbody>
            </table>
             {!! $students->render() !!}
        </div>
    </div>
</div>
</section>
@endsection







