@extends('layouts.app')
@section('content')
<section class="content-header">
 @include('partials.message-block')
      <h1>
        Perdoruesit
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
     
        <!-- text input -->
        <div class="form-group">
        <div class="box-header with-border">
          <h3 class="box-title">Update orari</h3>
        </div>
        <div class="box-body">
         <form action="{{route('updateSchedules', $schedules->id)}}" method="POST">
         <div class="form-group">
            <label>Klasa</label>
              <input class="form-control" type="text" name="clas" value="{{$schedules->clas->class}}/{{$schedules->clas->parallel}}" disabled/>
              <input type="hidden" name="_token" value="{!! csrf_token() !!}">
         </div>
          <div class="form-group">
            <label>Lenda</label>
              <select name="subject" id="clas" class="selectpicker form-control" data-show-subtext="true" data-live-search="true" value="{{$schedules->subject_id}}">
             @foreach($subjects as $sub)
                <option value="{{$sub->id}}">{{$sub->name}}</option>
              @endforeach
              </select>
         </div>
         <div class="form-group">
            <label>Profesori</label>
              <select name="professor" id="professor" class="selectpicker form-control" data-show-subtext="true" data-live-search="true" value="{{$schedules->professor_id}}">
             @foreach($professor as $prof)
                <option value="{{$prof->id}}">{{$prof->name}} {{$prof->lastname}} </option>
              @endforeach
              </select>            
         </div> 
         <div class="form-group">
            <label>Dita</label>
              <select name="dita" id="dita" class="selectpicker form-control" data-show-subtext="true" data-live-search="false" value="{{$schedules->dita}}">
                <option value="1">E Hëne</option>
                <option value="2">E Marte</option>
                <option value="3">E Mërkure</option>
                <option value="4">E Enjte</option>
                <option value="5">E Premte</option>
              </select>        
         </div>  
         <div class="form-group">
            <label>Koha e fillimit</label>
              <input class="form-control" type="text" name="koha_fillimit" id="koha_fillimit" value="{{$schedules->koha_fillimit}}"/>      
         </div>  
         <div class="form-group">
            <label>Koha e mbarimit</label>
              <input class="form-control" type="text" name="koha_mbarimit" id="koha_mbarimit" value="{{$schedules->koha_mbarimit}}"/>      
         </div> 
                <button class="btn btn-primary" type="submit">Ndrysho</button>
                <a href="{{route('homeSchedules')}}" class="btn btn-primary">Lista e orareve</a>
         
    </form>
</div>
        </div>
</div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">
    $('#koha_fillimit').timepicker();
    $('#koha_mbarimit').timepicker();


</script>

@endsection