@extends('layouts.app')
@section('content')
<section class="content-header">
 @include('partials.message-block')
      <h1>
        Perdoruesit
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
     
        <!-- text input -->
        <div class="form-group">
        <div class="box-header with-border">
          <h3 class="box-title">Title</h3>
        </div>
        <div class="box-body">
         <form action="{{route('updateStudents', $students->id)}}" method="POST">
         <div class="form-group">
            <label>Emri dhe mbiemri</label>
              <input class="form-control" type="text" name="name" value="{{$students->name}}" />
              <input type="hidden" name="_token" value="{!! csrf_token() !!}">
         </div>
         <div class="form-group">
            <label>Emri i prindit</label>
              <select name="parentName" id="parentName" class="selectpicker form-control" data-show-subtext="true" data-live-search="true" value="{{$students->name}}">
             @foreach($parents as $parent)
                <option value="{{$parent->id}}">{{$parent->name}} {{$parent->lastname}}</option>
              @endforeach
              </select>            
         </div> 
         <div class="form-group">
            <label>Vendlindja</label>
              <input class="form-control" type="text" name="birthplace" value="{{$students->birthplace}}">             
         </div> 
         <div class="form-group">
            <label>Vendbanimi</label>
              <input class="form-control" type="text" name="place" value="{{$students->place}}"/>         
         </div>
         <div class="form-group">
            <label>Datelindja</label>
              <input class="form-control" id="datapicker" name="birthday" value="{{$students->birthday}}"/>      
         </div>  
         <div class="form-group">
            <label>Gjinia</label> 
            <input type="radio" value="0"  name="gender"
                     @if($students->gender == '0') checked @endif>M
            <input type="radio" value="1" name="gender" style="margin-left: 10px;"
                     @if($students->gender == '1') checked @endif>F
         </div> 
         <form action="{{route('updateStudents', $students->id)}}" method="POST">
                <input type="hidden" name="_token" value="{{csrf_token()}}" />
                <button class="btn btn-primary" type="submit">Ndrysho</button>
                <a href="{{route('homeStudents')}}" class="btn btn-primary">Lista e Nxenesve</a>
         </form>
    </form>
</div>
        </div>
</div>
</section>
@endsection

