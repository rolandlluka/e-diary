<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{URL::asset('dist/css/AdminLTE.min.css')}}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{URL::asset('plugins/iCheck/square/blue.css')}}">
  <style>
      body {
          overflow-x: hidden;
          overflow-y: hidden;
      }
      #success_message{ display: none;}
    </style>
</head>
<body class="hold-transition login-page">

<div class="login-box">

  <div class="login-logo">
    <a href="{{route('login')}}"><b>Sistemi per menaxhimin e nxenesve</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
  @include('partials.message-block')
    <p class="login-box-msg">Logohu ketu</p>

    <form id="login_form" action="{{route('user.login')}}" method="post" >
    {{ csrf_field() }}
      <div class="form-group">
        <div class="col-md-14 inputGroupContainer">
          <div class="input-group">
              <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
              <input name="email" id="email" placeholder="E-Mail" class="form-control"  type="text">
          </div>
        </div>
      </div>

      <div class="form-group">
        <div class="col-md-14 inputGroupContainer">
          <div class="input-group">
              <span class="input-group-addon"><i class="glyphicon glyphicon-eye-close"></i></span>
              <input id="password"   type="password" name="password" placeholder="Password" class="form-control" >
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label> 
              <input id="remember" name="remember" type="checkbox" value="First Choice" tabindex="4" /> Remember Me
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Login</button>
          
        </div>
        <!-- /.col -->
      </div>
    </form>
    <!-- /.social-auth-links -->

     <a class="btn btn-link" href="#">Forgot Your Password?</a>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
<!-- jQuery 2.2.3 -->
{{ Html::script('plugins/jQuery/jquery-2.2.3.min.js') }}
<!-- Bootstrap 3.3.6 -->
{{ Html::script('bootstrap/js/bootstrap.min.js') }}
<!-- iCheck -->
{{ Html::script('plugins/iCheck/icheck.min.js') }}
{{ Html::script('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/js/bootstrapValidator.js') }}

<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });

    $('#login_form').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            
            email: {
                validators: {
                    notEmpty: {
                        message: 'Email-i juaj nuk eshte valid'
                    },
                    emailAddress: {
                        message: 'Email-i juaj nuk eshte valid'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'Passwordi juaj nuk eshte valid'
                    },
                    password: {
                        message: 'Passwordi juaj nuk eshte valid'
                    },
                    stringLength: {
                        message: 'Passwordi juaj duhet te jete se paku 6 karaktere',
                        min: function (value, validator, $field) {
                            return 6 - (value.match(/\r/g) || []).length;
                        }
                    }
                }
            }
            }
        })
        
  });

</script>

</body>

</html>
