@extends('layouts.app')

@section('content')
<section class="content-header">
  @include('partials.message-block')
    </section>
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
     
        <!-- text input -->
        <div class="form-group">
        <div class="box-header with-border">
          <h3 class="box-title">Title</h3>
        </div>
        <div class="box-body">
         <form method="post" action="{{route('updateUser', $user->id)}}">
         <div class="form-group">
            <label>Emri</label>
              <input class="form-control" type="text" name="name" value="{{$user->name}}" />
              <input type="hidden" name="_token" value="{!! csrf_token() !!}"/>
         </div>
          <div class="form-group">
            <label>Email</label>
              <input class="form-control" type="text" name="email" value="{{$user->email}}" />
         </div>
         <div class="form-group">
            <label>Roli</label>
              <select class="form-control" name="role_id">
                @foreach($roles as $role)
                    <option value="{{ $role->id }}" @if($role->id == $user->role_id) selected @endif>{{ $role->description }}</option>
                @endforeach
            </select>
             
         </div>  
         <div class="form-group">
            <label  for="newpassword">Passwordi i ri</label>
              <input type="password" class="form-control" name="newpassword" id="newpassword" placeholder="Passwordi i ri">
        
          </div>
          <div class="form-group">
            <label  for="password_confirmation">Konfirmo Passwordin</label>
              <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Konfirmo passwordin e ri">
          </div>  
         <button class="btn btn-primary" type="submit">Ndrysho</button>
         <a href="{{route('homeUser')}}" class="btn btn-primary">Kthehu Prapa</a>  
    </form>
</div>
        </div>
</div>
</section>

@endsection
