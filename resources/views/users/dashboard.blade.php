@extends('layouts.app')
<meta name="_token" content="{!! csrf_token()!!} "/>
@section('content')
<section class="content-header">
    @include('partials.message-block')
      <h1>
        Sistemi per menaxhimin e nxenesve
        <small>gjithcka fillon ketu</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Ballina</a></li>
        
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{$students}}</h3>

              <p>Nxënës</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-stalker"></i>
            </div>
            <a href="#" class="small-box-footer">Më shumë info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{$professors}}</h3>

              <p>Arsimtarë</p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-people"></i>
            </div>
            <a href="#" class="small-box-footer">Më shumë info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{$parents}}</h3>

              <p>Prinder</p>
            </div>
            <div class="icon">
              <i class="ion ion-android-contact"></i>
            </div>
            <a href="#" class="small-box-footer">Më shumë info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>{{$absences}}</h3>

              <p>Mungesa sot</p>
            </div>
            <div class="icon">
              <i class="ion ion-android-done"></i>
            </div>
            <a href="#" class="small-box-footer">Më shumë info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>

     <div class="row">
     <div class="col-md-8">
      <div class="box">
      <div class="box-header with-border">
      Klaset me mesataren me te mire
      </div>

      <div class="box-body">
      
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box-header with-border">
              
            </div>
            <div class="box-body chart-responsive">
              <div class="chart" id="revenue-chart" style="height: 300px;">


            </div>
            </div>
          <!-- /.nav-tabs-custom -->
      </div>
     </div> 
     </div>
      <div class="col-md-4">
      <div class="box">
      <div class="box-header with-border">
      Njoftime <button type="button" class="button-xs btn-default pull-right" id="add" value="add"><i class="fa fa-plus"></i></button>
      </div>

      <div class="box-body">
       <table class="table table-hover" id="noticeTable">
        <thead></thead>
        <tbody>
        @foreach($notices as $key => $notice)
        <tr id="notice{{$notice->id}}">
          <td>{{$notice->pershkrimi}}</td>
          <td>{{$notice->role}}</td>
          <td>{{$notice->koha_fillimit}}</td>
          <td>{{$notice->koha_mbarimit}}</td>
          <td>
          <button class="button-xs btn-default btn-edit" data-id="{{$notice->id}}"><i class="fa fa-edit"></i></button>
          <button class="button-xs btn-danger btn-delete" data-id="{{$notice->id}}"><i class="fa fa-remove"></i></button>
          </td>
        </tr>
        @endforeach
        </tbody>
        </table>
      </div>
     </div> 
     </div>
     </div>

     <div class="modal fade" id="notice" tabindex="-1" role="dialog">
        <div class="modal-dialog ">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Shto Njoftim</h4>
        </div>
        <div class="modal-body">
          <form  action="{{ route('new.notice') }}" method="post" id="frmNotice">
            

            <div class="form-group">
            <label for="pershkrimi">Pershkrimi</label>
              <input type="text" name="pershkrimi" id="pershkrimi" placeholder="Pershkrimi" required="" class="form-control">
            </div>
            <div class="form-group">
            <label for="roli" >Grupi i perdoruesve</label>
            <select  name="role" id="role" class="form-control input-sm">
                <option value="1">Nxenesit - Prinderit</option>
                <option value="2">Profesoret</option>
                <option value="3">Te gjithe perdoruesit</option>
            </select>
            </div>
            <div class="form-group">
            <label for="koha_fillimit">Nga data:</label>
              <input type="text" name="koha_fillimit" id="koha_fillimit"  class="form-control">
            </div>
            <div class="form-group">
            <label for="koha_mbarimit">Deri me daten:</label>
              <input type="text" name="koha_mbarimit" id="koha_mbarimit"  class="form-control">
            </div>
            <input type="hidden" name="id" id="id" value="">
        <div class="modal-footer">
          <input type="submit" value="Save" id="save" class="btn btn-primary">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          
        </div>
       
        </form>
      </div>
    </div>
      </div>
  </div>
</section>
@endsection

@section('scripts')

<script type="text/javascript">


$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
    })
      $('#add').on('click',function(){
            $('#save').val('save'); 
            $('#frmNotice').trigger('reset'); 
            $('#notice').modal('show');  
      })
         
      $('#frmNotice').on('submit',function(e){
            e.preventDefault();
            var form=$('#frmNotice');
            var formData=form.serialize();
            var url =form.attr('action');
            var state=$('#save').val();
            var type = 'post';
            if (state=='update'){
                type = 'put';
            }
            $.ajax({
                    type : type,
                    url : url,
                    data : formData,
                    success:function(data){
                    
                    var row='<tr id="notice'+ data.id +'">'+
                            '<td>'+ data.id +'<td>'+
                            '<td>'+ data.pershkrimi +'<td>'+
                            '<td>'+ data.role +'<td>'+
                            '<td>'+ data.koha_fillimit +'<td>'+
                            '<td>'+ data.koha_mbarimit +'<td>'+
                            '<td><button-xs class="btn btn-success btn-edit" data-id="'+ data.id+ '"></button>'+
                            '<button-xs class="btn btn-danger btn-delete" data-id="'+ data.id+ '"></button></td>'+
                            '</tr>';
                        if (state=='save') {
                            $('tbody').append(row);
                        }else{
                             $('#notice'+data.id).replaceWith(row);   
                        }
                        $('#frmNotice').trigger('reset');
                        $('#notice').focus();
                    }
            });
      })
    $( function() {
    $( "#koha_fillimit" ).datepicker({  format: 'yyyy-mm-dd', locale: 'sq'});
    $( "#koha_mbarimit" ).datepicker({  format: 'yyyy-mm-dd', locale: 'sq' });
    } );
      //-------shto rreshtat-----

      function addRow(data){
        var servis="";
        
        var row='<tr id="notice'+ data.id +'">'+
                '<td>'+ data.id +'<td>'+
                '<td>'+ data.pershkrimi +'<td>'+
                '<td>'+ data.role +'<td>'+
                '<td>'+ data.koha_fillimit +'<td>'+
                '<td>'+ data.koha_mbarimit +'<td>'+
                '<td><button class="btn btn-success btn-edit"></button>'+
                '<button class="btn btn-danger btn-delete"></button></td>'+
                '</tr>';
            $('tbody').append(row);
      }
      //-------update-------

      $('tbody').delegate('.btn-edit', 'click', function(){

            var value = $(this).data('id');
            var url = '{{route('get.update')}}' ;
            $.ajax({
                type : 'get',
                url : url,
                data : {'id':value},
                success:function(data){
                    $('#id').val(data.id);
                    $('#pershkrimi').val(data.pershkrimi);
                    $('#role').val(data.role);
                    $('#koha_fillimit').val(data.koha_fillimit);
                    $('#koha_mbarimit').val(data.koha_mbarimit);
                    $('#save').val('update');
                    $('#notice').modal('show');

                }
            });
      });
      $('tbody').delegate('.btn-delete', 'click', function(){
            var value = $(this).data('id');
            var url = ' {{route('del.notice')}}';
            if (confirm('A jeni te sigurte?')==true) {
                $.ajax({
                    type : 'post',
                    url : url,
                    data : {'id':value},
                    success:function(data){
                        alert(data.sms);
                        $('#notice'+value).remove();
                    }
                });
            }
      });
    Morris.Donut({
      element: 'revenue-chart',
      data: [
      @foreach($avg as $avg)
        {label:'{{$avg->clas->class}}/ {{$avg->clas->parallel}}', value: {{$avg->average}}},

      @endforeach
      ]
    });
</script>
@endsection
