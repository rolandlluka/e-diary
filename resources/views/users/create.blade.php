@extends('layouts.app')

@section('content')
<section class="content-header">
  @include('partials.message-block')
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
     
        <!-- text input -->
        <div class="form-group">
        <div class="box-header with-border">
          
        </div>
        <div class="box-body">
         <form action="{{route('storeUser')}}" method="POST">
         <div class="form-group">
            <label>Emri</label>
              <input class="form-control" type="text" name="name" />
              <input type="hidden" name="_token" value="{!! csrf_token() !!}">
         </div>
          <div class="form-group">
            <label>Email</label>
              <input class="form-control" type="text" name="email" />
         </div>
         <div class="form-group">
            <label>Password</label>
              <input class="form-control" type="password" name="password" />
         </div>   
         <button class="btn btn-primary" type="submit">Regjistro</button>
         <a href="{{route('homeUser')}}" class="btn btn-primary">Kthehu Prapa</a>
    </form>
</div>
        </div>
</div>
</section>
@endsection
