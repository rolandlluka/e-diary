@extends('layouts.app')

@section('content')

   <section class="content-header">
   @include('partials.message-block')
      <h1>
       Perdoruesit
      </h1>
  </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><a href="{{route('createUser')}}" class="btn btn-primary">Shto Perdorues</a></h3>
        </div>
        <div class="box-body">
          <div class="box-body table-responsive no-padding">
              <table class="table table-hover">

                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                    <tbody>

                        @foreach($users as $user)
                            <tr>
                                <td>
                                    {{ $user->name }}
                                </td>
                                <td>{{ $user->email }}</td>

                            <td>
                            
                             <form action="{{route('deleteUser', $user->id)}}" method="POST">
                             <a class="glyphicon glyphicon-pencil" href="{{route('singleUser', $user->id)}}">Edit </a>&nbsp;&nbsp;
                              <input type="hidden" name="_token" value="{{csrf_token() }}">  
                              <button type="submit" class="btn btn-xs btn-danger" onclick="return confirm('Are you sure you want to delete this tem?')">Delete</button> 
                              </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
</section>
@endsection
