@extends('layouts.app')

@section('stylesheets')
    <link rel="stylesheet" href="{{ asset('plugins/datepicker/css/bootstrap-datepicker.min.css') }}"
@stop

@section('content')
<section class="content-header">
      <h1>
        Shto nxenes
      </h1>
      <ol class="breadcrumb">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
     
        <!-- text input -->
        <div class="form-group">
        <div class="box-header with-border">
          
        </div>
        <div class="box-body">
         <form action="{{route('storeSubjects')}}" method="POST">
         <input type="hidden" name="_token" value="{!! csrf_token() !!}">
         <div class="form-group">
            <label>Lenda</label>
             <input class="form-control" type="text" name="name"/>
            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
         </div>
          <button class="btn btn-primary" type="submit">Regjistro</button>
          <a href="{{route('homeSubjects')}}" class="btn btn-primary">Lista e lendeve</a>
    </form>
   </div>
 </div>
</div>
</section>
@endsection

