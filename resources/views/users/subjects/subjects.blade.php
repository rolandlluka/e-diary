@extends('layouts.app')
@section('content')

<section class="content-header">
@include('partials.message-block')
      <h1>
       Lendet
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Ballina</a></li>
        
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <a href="{{route('createSubjects')}}" class="btn btn-primary">Shto Lendet</a>
        </div>
        <div class="box-body">
          <div class="box-body table-responsive no-padding">
              <table class="table table-hover">

                <thead>
                    <tr>
                       <th>Lenda</th>
                        <th>Profesori</th>
                        <th>Edito</th>
                     </tr>
                </thead>
                <tbody>
                @foreach($subjects as $subject)
                <tr>
                    <td>
                        {{ $subject->name }}
                    </td>
                    <td>@foreach($subject->professor as $professors)
                            <span class="badge">{{ $professors->name }}</span>
                        @endforeach
                    </td>
                    <td>
                <form action="{{route('deleteSubjects', $subject->id)}}" method="POST">
                    <input type="hidden" name="_token" value="{{csrf_token() }}"/>
                    <a class="glyphicon glyphicon-pencil"
                       href="{{route('editSubjects', $subject->id)}}">Edit </a>
                    <button type="submit" class="btn btn-xs btn-danger"
                    onclick="return confirm('A jeni të sigurtë qe doni ta fshini?');">
                    Delete
                    </button>
                </form>
                    </td>
                </tr>
                    @endforeach
                </tbody>
            </table>
            {!! $subjects->render() !!}
        </div>
    </div>
</div>
</section>
@endsection
