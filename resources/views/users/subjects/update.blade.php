@extends('layouts.app')

@section('stylesheets')
    <link rel="stylesheet" href="{{ asset('plugins/datepicker/css/bootstrap-datepicker.min.css') }}"
@stop

@section('content')
<section class="content-header">
      <h1>
        Perdoruesit
      </h1>
      <ol class="breadcrumb">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
      </ol>
    </section>
    <section class="content">
            <div class="form-group">
            <div class="box-header with-border">
              <h3 class="box-title">Title</h3>
            </div>
            <div class="box-body">
              <form action="{{route('updateSubjects', $subjects->id)}}" method="POST">
              <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                 <div class="form-group">
                    <label>Emri i lendes</label>
                      <input class="form-control" type="text" name="name" value="{{$subjects->name}}"/>
                 </div>
                         <form action="{{route('updateSubjects', $subjects->id)}}" method="POST">
                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    <button class="btn btn-primary" type="submit">Ndrysho</button>
                    <a href="{{route('homeSubjects')}}" class="btn btn-primary">Kthehu Prapa</a>
                </form>
            </form>
        </div>
    </div>
</div>
</section>
@endsection

