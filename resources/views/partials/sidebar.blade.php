      
      
<aside class="main-sidebar">
    <section class="sidebar">
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Filan Fisteku</p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
     <ul class="sidebar-menu">
        <li class="header">HEADER</li>
        <!-- Optionally, you can add icons to the links -->
        @if(Auth::user()->role_id == 1)
        <li class="{{ Request::is('admin/users/dashboard*') ? 'active' : '' }}"><a href="{{route('adminDashboard')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a></li>
        <li class="{{ Request::is('admin/users') ? 'active' : '' }}"><a href="{{route('homeUser')}}"><i class="fa fa-users"></i> <span>Perdoruesit</span>
        </a></li>
        <li class="{{ Request::is('admin/schedules*') ? 'active' : '' }}"><a href="{{route('homeSchedules')}}"><i class="fa fa-calendar"></i>Oraret</a></li>
        <li class="{{ Request::is('admin/class*') ? 'active' : '' }}"><a href="{{route('homeClass')}}"><i class="fa fa-home"></i>Klasat</a></li>
        <li class="{{ Request::is('admin/parents*') ? 'active' : '' }}"><a href="{{route('homeParents')}}"><i class="fa fa-user"></i>Prinderit</a></li>
        <li class="{{ Request::is('admin/students*') ? 'active' : '' }}"><a href="{{route('homeStudents')}}"><i class="fa fa-user-md"></i>Nxenesit</a></li>
        <li class="{{ Request::is('admin/subjects*') ? 'active' : '' }}"><a href="{{route('homeSubjects')}}"><i class="fa fa-book"></i>Lendet</a></li>
        <li class="{{ Request::is('admin/professor') ? 'active' : '' }}"><a href="{{route('homeProfessor')}}"><i class="fa fa-user-secret"></i>
        Professoret</a></li>
        <li class="{{ Request::is('admin/professor/programi') ? 'active' : '' }}"><a href="{{route('getProgram')}}"><i class="fa fa-list-ol"></i>
        Programi Mesimor</a></li>
        @elseif(Auth::user()->role_id == 2)
        <li class="{{ Request::is('student') ? 'active' : '' }}"><a href="{{route('studentDashboard')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a></li>
        <li class="{{ Request::is('student/schedules*') ? 'active' : '' }}"><a href="{{route('homeSchedule', Auth::user()->student_id)}}"><i class="fa fa-calendar"></i> <span>Orari</span>
        </a></li>
        <li class="{{ Request::is('students/marks') ? 'active' : '' }}"><a href="{{route('homeMark', Auth::user()->student_id)}}"><i class="fa fa-pencil"></i>Notat</a></li>
        <li class="{{ Request::is('students/marks/absence*') ? 'active' : '' }}"><a href="{{route('homeAbsence', Auth::user()->student_id)}}"><i class="fa fa-check-square-o"></i>Mungesat</a></li>
        @elseif(Auth::user()->role_id == 3)
        <li class="{{ Request::is('professor*') ? 'active' : '' }}"><a href="{{route('professorDashboard')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a></li>
        <li class="{{ Request::is('professor/ora') ? 'active' : '' }}"><a href="{{route('oraMesimore')}}"><i class="fa  fa-list-ol"></i>Sheno oren</a></li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-pencil"></i>
            <span>Notat</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ Request::is('professor/marks') ? 'active' : '' }}"><a href="{{route('professorMarks')}}"><i class="fa fa-circle-o"></i>Shto Nota</a></li>
            <li class="{{ Request::is('professor/marks/edit*') ? 'active' : '' }}"><a href="{{route('editMarks')}}"><i class="fa fa-circle-o"></i>Ndrysho Notat</a></li>
            <li class="{{ Request::is('professor/marks/list*') ? 'active' : '' }}"><a href="{{route('addprofessormarks')}}"><i class="fa fa-circle-o"></i>Lista e Notave</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-check-square-o"></i>
            <span>Mungesat</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ Request::is('professor/marks') ? 'active' : '' }}"><a href="{{route('absence')}}"><i class="fa fa-circle-o"></i>Shto Mungesa</a></li>
            <li class="{{ Request::is('professor/marks/editabsence*') ? 'active' : '' }}"><a href="{{route('editAbsence')}}"><i class="fa fa-circle-o"></i>Ndrysho Mungesat</a></li>
            <li class="{{ Request::is('professor/marks/list/absence*') ? 'active' : '' }}"><a href="{{route('professorAbsences')}}"><i class="fa fa-circle-o"></i>Lista e mungesave</a></li>
          </ul>
        </li>
        @else
        <li class="{{ Request::is('parent/') ? 'active' : '' }}"><a href="{{route('parentDashboard')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a></li>
        <li class="{{ Request::is('parent/marks*') ? 'active' : '' }}"><a href="{{route('parentStudents')}}"><i class="fa fa-pencil"></i>Notat</a></li> 
        <li class="{{ Request::is('parent/marks/absences') ? 'active' : '' }}"><a href="{{route('getchildren')}}"><i class="fa fa-check-square-o"></i>Mungesat</a></li>
      </ul>
      @endif
    </section>
  </aside>

addprofessormarks
  