@extends('layouts.app')

@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Sistemi per menaxhimin e nxenesve
        <small>gjithcka fillon ketu</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Ballina</a></li>
        
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

     <div class="row">
     <div class="col-md-8">
      <div class="box">
      <div class="box-header with-border">
      Informacione per femijet tuaj
      </div>

      <div class="box-body">
      </div>
     </div> 
     </div>
      <div class="col-md-4">
      <div class="box">
      <div class="box-header with-border">
      Njoftime
      </div>

      <div class="box-body">
       <table class="table table-hover" id="noticeTable">
        <thead></thead>
        <tbody>
        @foreach($notices as $key => $notice)
        <tr id="notice{{$notice->id}}">
          <td>{{$notice->pershkrimi}}</td>
          <td>{{$notice->koha_fillimit}}</td>
          <td>{{$notice->koha_mbarimit}}</td>
        </tr>
        @endforeach
        </tbody>
        </table>
      </div>
     </div> 
     </div>
     </div>
</section>

@endsection
