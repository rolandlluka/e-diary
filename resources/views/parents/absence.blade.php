@extends('layouts.app')

@section('content')
<section class="content-header">
<h1>Pasqyrja e mungesave</h1>
      <ol class="breadcrumb">
      
        <li><a href="#"><i class="fa fa-dashboard"></i> Ballina</a></li>
        
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          
        </div>
        <div class="box-body">
          <div class="box-body table-responsive no-padding">
              <table class="table table-hover">

                <thead>
                    <tr>
                       <th>Klasa</th>
                       <th>Lenda</th>
                       <th>Koha</th>
                       <th>Mungesa</th>
                       <th>Gjysemvjetori</th>
                        <th></th>
                     </tr>
                </thead>
                <tbody>
                @foreach($absence as $abs)
                <tr>
                  <td>{{$abs->clas->class}}/{{$abs->clas->parallel}}</td>
                  <td>{{$abs->subject->name}}</td>
                  <td>{{$abs->koha}}</td>
                  @if($abs->arsye == '0')
                      <td>E paarsyeshme</td>
                  @else
                      <td>E arsyeshme</td>
                  @endif
                  <td>{{$abs->semester}}</td>
                  <td> <button class="btn btn-info">Print</button> </td>
                </tr>

                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Mungesat sipas lendeve</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Klasa/Paralelja</th>
                    <th>Lenda</th>
                    <th>Mungesa te arsyeshme</th>
                    <th>Mungesa te paarsyeshme</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($avg as $avg)
                  <tr>
                  
                    <td><a href="#">{{$avg->clas->class}}/{{$avg->clas->parallel}}</a></td>
                    <td>{{$avg->subject->name}}</td>
                    <td>{{$avg->arsyeshme}}</td>
                    <td>{{$avg->paarsyeshme}}</td>
                    <td></td>
                  </tr>
                   @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
          
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <a href="{{route('printabsence', $students)}}" class="btn btn-sm btn-info btn-flat pull-left" target="_blank" >Printo Mungesat</a>
            </div>
            
            <!-- /.box-footer -->
          </div>
</section>
@endsection
