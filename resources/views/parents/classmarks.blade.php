@extends('layouts.app')

@section('content')
<section class="content-header">
<h1>Notat</h1>
      <ol class="breadcrumb">
      
        <li><a href="#"><i class="fa fa-dashboard"></i> Ballina</a></li>
        
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
<div class="box ">
            <div class="box-header with-border">
              <h3 class="box-title">Notat mesatare</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Nxenesi</th>
                    <th>Lenda</th>
                    <th>Notat</th>
                    <th>Nota mesatare</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($avg as $avg)
                  <tr>
                  
                    <td><a href="#">{{$avg->student->name}} {{$avg->student->lastname}}</a></td>
                    <td>{{$avg->subject->name}}</td>
                    <td>{{$avg->notat}}</td>
                    <td>
                      {{$avg->average}}
                    </td>
                   
                  </tr>
                   @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
               <a href="{{route('printmarks', $student->id)}}" class="btn btn-sm btn-info btn-flat pull-left" target="_blank" >Printo Suksesin</a>
            </div>
            <!-- /.box-footer -->
          </div>
</section>
@endsection
