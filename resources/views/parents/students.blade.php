@extends('layouts.app')

@section('content')
<section class="content-header">
<h1>Notat</h1>
      <ol class="breadcrumb">
      
        <li><a href="#"><i class="fa fa-dashboard"></i> Ballina</a></li>
        
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          
        </div>
        <div class="box-body">
          <div class="box-body table-responsive no-padding">
              <table class="table table-hover">

                <thead>
                    <tr>
                       <th><span>Femijet tuaj</span> </th>
                     </tr>
                </thead>
                <tbody>
                @foreach($students as $student)
                <tr>
                  <td><a href="{{route('studentMark', $student->id)}}" class="user-link">{{ $student->name }}</a></td>
                </tr>

                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
</section>
@endsection
