@extends('layouts.app')

@section('content')
<section class="content-header">
<h1>Lista e femijeve tuaj</h1>
      <ol class="breadcrumb">
      
        <li><a href="#"><i class="fa fa-dashboard"></i> Ballina</a></li>
        
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          
        </div>
        <div class="box-body no-padding">
        <ul class="users-list clearfix">
        @foreach($students as $student)
              <li>
                <img src="{{URL::asset('dist/img/avatar1.png')}}" alt="User Image">
                <a class="users-list-name" href="{{route('getbyabsence', $student->id)}}">{{ $student->name }} {{ $student->lastname }}</a>
                <span class="users-list-date">Today</span>
              </li>
        @endforeach
        </ul>
    </div>
</div>
</section>
@endsection
