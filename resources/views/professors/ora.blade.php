@extends('layouts.app')

@section('content')
<section class="content-header">
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
         <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Sheno oren</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form action="{{route('storeOra')}}" method="POST" role="form">
              <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <!-- text input -->
                <div class="col-md-6">
                <div class="form-group">
                <label>Lenda</label>
                  <select name="subject" id="subject" class="selectpicker form-control" data-show-subtext="true" data-live-search="true">
                    <option value="" disabled selected>Lenda</option>
                   @foreach($professors as $prof)
                      <option value="{{$prof->subject_id}}">{{$prof->subject->name}}</option>
                    @endforeach
                    </select>
                </div>
                <div class="form-group">
                  <label>Klasa/Paralelja</label>
                  <select name="class" id="class" class="selectpicker form-control" data-live-search="true" required="">
              <option value="" disabled selected></option>
              @foreach($class as $cl)
                <option value="{{$cl->id}}"> {{$cl->class}}/{{$cl->parallel}} </option>
              @endforeach
              </select>
                </div>

                <div class="form-group">
                <label>Ora e mesimit</label>
                  <input type='text'  class="form-control" id='datetimepicker4' name="ora" required="">
                </div>
                </div>
                <div class="col-md-6">
                <div class="form-group">
                  <label>Programi per kete ore</label>
                  <textarea class="form-control" rows="8" name="pershkrimi" placeholder="Shkruaj dicka ..." required=""></textarea>
                </div>
                </div>
                <div class="col-md-6">
                <button class="btn btn-primary" type="submit">Ruaj</button>
           <a href="{{route('professorMarks')}}" class="btn btn-primary">Kthehu Prapa</a>
                </div>
              </form>
            </div>
            <!-- /.box-body -->
          </div>

</section>
@endsection
