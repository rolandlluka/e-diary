@extends('layouts.app')

@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Sistemi per menaxhimin e nxenesve
        <small>gjithcka fillon ketu</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Ballina</a></li>
        
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
 <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>23</h3>

              <p>Nxënës</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-stalker"></i>
            </div>
            <a href="#" class="small-box-footer">Më shumë info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{$subject}}</h3>

              <p>Lende te ligjeruara</p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-people"></i>
            </div>
            <a href="#" class="small-box-footer">Më shumë info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{$marks}}</h3>

              <p>Nota te vena sot</p>
            </div>
            <div class="icon">
              <i class="ion ion-android-contact"></i>
            </div>
            <a href="#" class="small-box-footer">Më shumë info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>{{$absences}}</h3>

              <p>Mungesa sot</p>
            </div>
            <div class="icon">
              <i class="ion ion-android-done"></i>
            </div>
            <a href="#" class="small-box-footer">Më shumë info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>

     <div class="row">
     <div class="col-md-8">
      <div class="box">
      <div class="box-header with-border">
      Statistika
      </div>

      <div class="box-body">
      </div>
     </div> 
     </div>
      <div class="col-md-4">
      <div class="box">
      <div class="box-header with-border">
      Njoftime
      </div>

      <div class="box-body">
       <table class="table table-hover" id="noticeTable">
        <thead></thead>
        <tbody>
        @foreach($notices as $key => $notice)
        <tr id="notice{{$notice->id}}">
          <td>{{$notice->pershkrimi}}</td>
          <td>{{$notice->koha_fillimit}}</td>
          <td>{{$notice->koha_mbarimit}}</td>
        </tr>
        @endforeach
        </tbody>
        </table>
      </div>
     </div> 
     </div>
     </div>
</section>

@endsection
