@extends('layouts.app')

@section('content')
<section class="content-header">
<h1>Notat</h1>
      <ol class="breadcrumb">
      
        <li><a href="#"><i class="fa fa-dashboard"></i> Ballina</a></li>
        
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          
        </div>
        <div class="box-body">
          <div class="box-body table-responsive no-padding">
              <table class="table table-hover">

                <thead>
                    <tr>
                       <th>Klasa/Paralelja</th>
                       <th>Studenti</th>
                       <th>Nota</th>
                        <th>Lenda</th>
                        <th>Semestri</th>
                     </tr>
                </thead>
                <tbody>
                @foreach($marks as $mark)
                <tr>
                  <td>{{$mark->clas->class}}/{{$mark->clas->parallel}}</td>
                  <td>{{$mark->student->name}} {{$mark->student->lastname}}</td>
                  <td>{{$mark->mark}}</td>
                  <td>{{$mark->subject->name}}</td>
                  <td>{{$mark->semester}}</td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
</section>
@endsection