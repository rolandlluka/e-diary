@extends('layouts.app')

@section('content')
<section class="content-header">
      <ol class="breadcrumb">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
     
        <!-- text input -->
        <div class="form-group">
        </div>
        <div class="box-body">
         <form action="{{route('updateMarks', $marks->id)}}" method="POST">
         <input type="hidden" name="_token" value="{!! csrf_token() !!}">
         <div class="form-group">
            <label>Nota</label>
              <input class="form-control" type="text" name="mark" value="{{$marks->mark}}"/>
         </div>
            <button class="btn btn-primary" type="submit">Ruaje</button>
            <a href="{{route('editMarks')}}" class="btn btn-primary">Lista e Notave</a>
        </form> 
</div>
        </div>
</div>
</section>
@endsection
