@extends('layouts.app')

@section('content')
<section class="content-header">
<h1>Notat per test</h1>
      <ol class="breadcrumb">
      
        <li><a href="#"><i class="fa fa-pencil"></i> Notat per individ</a></li>
        
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <form action="{{route('storeTestmarks')}}" method="POST" class="form-horizontal">
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
<div class="row clearfix">

    <div class="col-md-12 column">
    
      <table class="table table-bordered table-hover" id="tab_logic">
        <tbody>
          <tr id='addr0'>
            <td>
            1
            </td>
            <td>
            <select name="class[]" id="class" class="form-control">
              <option value="" disabled selected></option>
              @foreach($class as $cl)
                <option value="{{$cl->id}}"> {{$cl->class}}/{{$cl->parallel}} </option>
              @endforeach
            </select>
            </td>
            <td>
              <select name="student[]" id="student" class="form-control">
              <option value="" style="display:none;"></option>
            </select>
            </td>
            <td>
            <select name="subject[]" id="subject[]" class="form-control">
              <option value="" disabled selected>Lenda</option>
             @foreach($professors as $prof)
                <option value="{{$prof->subject_id}}">{{$prof->subject->name}}</option>
              @endforeach
           </select>
            </td>
            <td>
            <select name='mark[]' id='mark[]' class="form-control">
              <option value="0" disabled selected></option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
             </select>
            </td>
          </tr>
         <tr id='addr1'></tr>
        </tbody>
      </table>
     
    </div>
  </div>
  <a id="add_row" class="btn btn-default pull-left">Shto nxenes</a>
  <a id='delete_row' class="pull-right btn btn-danger">Fshij Nxenes</a>
  <button class="btn btn-primary" type="submit">Ruaj</button>
  </form>
</section>
@endsection

@section('scripts')
<script type="text/javascript">
  
       $(document).ready(function(){
      var i=1;
     $("#add_row").click(function(){
      $('#addr'+i).html("<td>"+ (i+1) +"</td><td><select name='class[]' id='class"+ (i) +"' class='form-control' onchange='studenti("+ i +")'><option value='' disabled selected></option>@foreach($class as $cl)<option value='{{$cl->id}}'>{{$cl->class}}/{{$cl->parallel}}</option>@endforeach</select></td><td><select name='student[]' id='student"+ (i) +"' class='form-control'><option value='' style='display:none;'></option></select></td><td><select name='subject[]' id='subject[]' class='form-control'><option value='' disabled selected>Lenda</option>@foreach($professors as $prof)<option value='{{$prof->subject_id}}'>{{$prof->subject->name}}</option>@endforeach</select></td><td><select name='mark[]' id='mark[]' class='form-control'><option value='0' disabled selected></option><option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option><option value='5'>5</option></select></td>");

      $('#tab_logic').append('<tr id="addr'+(i+1)+'"></tr>');
      i++; 
  });
     $("#delete_row").click(function(){
       if(i>1){
     $("#addr"+(i-1)).html('');
     i--;
     }
   });

});
    function studenti(id){

      var clas_id=$("#class"+ id +"").val();

        $.get('ajax-marks?clas_id=' +clas_id, function(data){

            $.each(data, function(index, studObj){

              $("#student"+ id +"").append('<option value="'+studObj.id+'">'+studObj.name+'<option>')
            });
        });
}

$('#class').on('change', function(e){
        console.log(e);

        var clas_id = e.target.value;
        $.get('ajax-marks?clas_id=' +clas_id, function(data){

           $('#student').empty();
            $.each(data, function(index, studObj){

              $('#student').append('<option value="'+studObj.id+'">'+studObj.name+'<option>')
            });
        });

      });
</script>  

@endsection