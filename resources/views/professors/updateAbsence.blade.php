@extends('layouts.app')

@section('content')
<section class="content-header">
      <ol class="breadcrumb">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
     
        <!-- text input -->
        <div class="form-group">
        </div>
        <div class="box-body">
         <form action="{{route('updateAbsence', $absences->id)}}" method="POST">
         <input type="hidden" name="_token" value="{!! csrf_token() !!}">
         <div class="form-group">
         <label>Arsyeshme/Paarsyeshme</label>
            <select name="arsye" class="form-control">
              <option value="{{$absences->arsye}}" disabled selected></option>
             
                <option value="1">E arsyeshme</option>
                <option value="0">E paarsyeshme</option>
             </select>
         </div>
         <div class="form-group">
         <label>Pershkrimi i mungeses</label>
            <textarea class="form-control" rows="4" name="pershkrimi" placeholder="Shkruaj ..." value="{{$absences->peshkrimi}}" ></textarea>
         </div>
         <div class="form-group">
          <button class="btn btn-primary" type="submit">Ndrysho</button>
            <a href="{{route('editAbsence')}}" class="btn btn-warning">Lista e Mungesave</a>
         </div>   
        </form> 
</div>
        </div>
</div>
</section>
@endsection
