@extends('layouts.app')

@section('content')
<section class="content-header">
<h1>Lista e mungesave</h1>
      <ol class="breadcrumb">
      
        <li><a href="#"><i class="fa fa-dashboard"></i> Ballina</a></li>
        
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          
        </div>
        <div class="box-body">
          <div class="box-body table-responsive no-padding">
              <table class="table table-hover" id="table-absence">

                <thead>
                    <tr>
                       <th>Klasa/Paralelja</th>
                       <th>Studenti</th>
                       <th>Lenda</th>
                        <th>Koha e mungeses</th>
                        <th>Mungesa</th>
                        <th>Gjysemvjetori</th>
                        <th></th>
                     </tr>
                </thead>
                <tbody>
                @foreach($absences as $absence)
                <tr>
                @if(isset($absence) && isset($absence->clas))
                  <td>{{$absence->clas->class or '' }}/{{$absence->clas->parallel or ''}}</td>  
                @endif

                @if(isset($absence) && isset($absence->clas))
                  <td>{{$absence->student->name or '' }} {{$absence->student->lastname or ''}}</td>
                @endif

                @if(isset($absence) && isset($absence->clas))
                  <td>{{$absence->subject->name or '' }}</td>
                @endif
                  <td>{{$absence->koha}}</td>
                  <td>@if ($absence->arsye ==0) 
                  E Paarsyeshme
                  @else
                  E arsyeshme
                  @endif</td>
                  @if($absence->semester == '1')
                      <td>I Parë</td>
                  @else
                      <td>I Dytë</td>
                  @endif
                  <form action="{{route('deleteAbsence', $absence->id)}}" method="POST">
                  <input type="hidden" name="_token" value="{{csrf_token() }}">  
                  <td><button type="submit" class="btn btn-xs btn-danger" onclick="return confirm('a jeni te sigurte?')">Delete</button></td>
                  </form>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
</section>
@endsection
@section('scripts')
<script>
  $(function () {
    $('#table-absence').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
</script>


@endsection