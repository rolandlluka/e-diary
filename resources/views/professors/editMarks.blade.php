@extends('layouts.app')

@section('content')
<section class="content-header">
<h1>Notat per individ</h1>
      <ol class="breadcrumb">
      
         <li><a href="#"><i class="fa fa-dashboard"></i> Ballina</a></li>
        
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
        <form action="#"  class="form-horizontal">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
        <div class="col-lg-3 col-xs-12">
            <label>Lenda</label>
              <select name="subject" id="subject" class="selectpicker form-control" data-show-subtext="true" data-live-search="true">
              <option value="" disabled selected>Lenda</option>
             @foreach($professors as $prof)
                <option value="{{$prof->subject_id}}">{{$prof->subject->name}}</option>
              @endforeach
              </select>
         </div>
         <div class="col-lg-3 col-xs-12">
            <label>Klasa</label>
              <select name="class" id="class" class="selectpicker form-control input-sm" data-live-search="true">
              <option value="" disabled selected></option>
              @foreach($class as $cl)
                <option value="{{$cl->id}}"> {{$cl->class}}/{{$cl->parallel}} </option>
              @endforeach
              </select>
         </div>
         <div class="col-lg-3 col-xs-12">
            <label>Nxenesi</label>
              <select name="student" id="student" class="form-control input-sm" data-show-subtext="true" data-live-search="true">
              <option value=""></option>
              </select>
         </div>
         <div class="col-lg-3 col-xs-12">
         <a class="btn btn-success" onclick="blini()" style="margin-top: 20px">Ndrysho </a>
         </div>
         </form>
         </div>
</section>
@endsection
@section('scripts')
  <script>
  function blini(){
    var subject=$('#subject').val();
    var student=$('#student').val();
    window.location = 'http://localhost/e-diary/public/professor/marks/getmark/'+subject+'/'+student+'';
  }

  $.ajaxSetup({
   headers: {
     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
   }

   });
      $('#class').on('change', function(e){
        console.log(e);

        var clas_id = e.target.value;
        $.get('ajax-student?clas_id=' +clas_id, function(data){

           $('#student').empty();
            $.each(data, function(index, studObj){

              $('#student').append('<option value="'+studObj.id+'">'+studObj.name+'<option>')
            });
        });

      });

  </script>
@endsection