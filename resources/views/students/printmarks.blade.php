<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
    </title>
    <style type="text/css">
      body { line-height:108%; font-family:Calibri; font-size:11pt }
      p { margin:0pt 0pt 8pt }
      table { margin-top:0pt; margin-bottom:8pt }
    </style>
  </head>
  <body>
    <div>
      <p style="text-align:center; line-height:108%; font-size:28pt">
        <strong><span style="font-size:28pt; ">Sistemi pë</span></strong><strong><span style="font-size:28pt; ">r menaxhimin e studentëve</span></strong>
      </p>
      <p style="text-align:center; line-height:108%; font-size:16pt">
        <strong><span style="font-size:16pt; ">Pasqyrja e suksesit te nxenesit</span></strong>
      </p>
      <p style="text-align:center; line-height:108%; font-size:16pt">
        <strong><span style="font-size:16pt; ">&#xa0;</span></strong>
      </p>
      <p style="text-align:center; line-height:108%; font-size:16pt">
        <strong><span style="font-size:16pt; ">&#xa0;</span></strong>
      </p>
      <p style="text-align:right; line-height:90%; font-size:14pt; padding-right:38px" >
        <span style="font-size:14pt">Studenti: {{Auth::user()->name}} {{Auth::user()->lastname}}</span>
      </p>
      <table cellspacing="0" cellpadding="0" style="border-collapse:collapse; margin:0 auto; width:486.65pt">
      <thead>
        <tr style="height:23.1pt">
          <th style="border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:0.75pt; border-left-color:#000000; border-left-style:solid; border-left-width:0.75pt; border-right-color:#000000; border-right-style:solid; border-right-width:0.75pt; border-top-color:#000000; border-top-style:solid; border-top-width:0.75pt; padding-left:5.03pt; padding-right:5.03pt; vertical-align:top; width:104.9pt">
            <p style="margin-bottom:0pt; text-align:center; line-height:normal; font-size:12pt">
              <strong><span style="font-family:Calibri; font-size:12pt; ">Klasa/Paralelja</span></strong>
            </p>
          </th>
          <th style="border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:0.75pt; border-left-color:#000000; border-left-style:solid; border-left-width:0.75pt; border-right-color:#000000; border-right-style:solid; border-right-width:0.75pt; border-top-color:#000000; border-top-style:solid; border-top-width:0.75pt; padding-left:5.03pt; padding-right:5.03pt; vertical-align:top; width:81.75pt">
            <p style="margin-bottom:0pt; text-align:center; line-height:normal; font-size:12pt">
              <strong><span style="font-family:Calibri; font-size:12pt; ">Lenda</span></strong>
            </p>
          </th>
          <th style="border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:0.75pt; border-left-color:#000000; border-left-style:solid; border-left-width:0.75pt; border-right-color:#000000; border-right-style:solid; border-right-width:0.75pt; border-top-color:#000000; border-top-style:solid; border-top-width:0.75pt; padding-left:5.03pt; padding-right:5.03pt; vertical-align:top; width:118.75pt">
            <p style="margin-bottom:0pt; text-align:center; line-height:normal; font-size:12pt">
              <strong><span style="font-family:Calibri; font-size:12pt; ">Notat</span></strong>
            </p>
          </th>
          <th style="border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:0.75pt; border-left-color:#000000; border-left-style:solid; border-left-width:0.75pt; border-right-color:#000000; border-right-style:solid; border-right-width:0.75pt; border-top-color:#000000; border-top-style:solid; border-top-width:0.75pt; padding-left:5.03pt; padding-right:5.03pt; vertical-align:top; width:137.3pt">
            <p style="margin-bottom:0pt; text-align:center; line-height:normal; font-size:12pt">
              <strong><span style="font-family:Calibri; font-size:12pt; ">Nota mesatare</span></strong>
            </p>
          </th>
        </tr>
        </thead>
        <tbody>
        @foreach($avg as $avg)
        <tr style="height:23.1pt">
          <td style="border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:0.75pt; border-left-color:#000000; border-left-style:solid; border-left-width:0.75pt; border-right-color:#000000; border-right-style:solid; border-right-width:0.75pt; border-top-color:#000000; border-top-style:solid; border-top-width:0.75pt; padding-left:5.03pt; padding-right:5.03pt; vertical-align:top; width:104.9pt">
            <p style="margin-bottom:0pt; text-align:center; line-height:normal; font-size:12pt">
              <span style="font-family:Calibri; font-size:12pt">{{$avg->clas->class}}/{{$avg->clas->parallel}}</span>
            </p>
          </td>
          <td style="border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:0.75pt; border-left-color:#000000; border-left-style:solid; border-left-width:0.75pt; border-right-color:#000000; border-right-style:solid; border-right-width:0.75pt; border-top-color:#000000; border-top-style:solid; border-top-width:0.75pt; padding-left:5.03pt; padding-right:5.03pt; vertical-align:top; width:81.75pt">
            <p style="margin-bottom:0pt; text-align:center; line-height:normal; font-size:12pt">
              <span style="font-family:Calibri; font-size:12pt">{{$avg->subject->name}}</span>
            </p>
          </td>
          <td style="border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:0.75pt; border-left-color:#000000; border-left-style:solid; border-left-width:0.75pt; border-right-color:#000000; border-right-style:solid; border-right-width:0.75pt; border-top-color:#000000; border-top-style:solid; border-top-width:0.75pt; padding-left:5.03pt; padding-right:5.03pt; vertical-align:top; width:118.75pt">
            <p style="margin-bottom:0pt; text-align:center; line-height:normal; font-size:12pt">
              <span style="font-family:Calibri; font-size:12pt">{{$avg->notat}}</span>
            </p>
          </td>
          <td style="border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:0.75pt; border-left-color:#000000; border-left-style:solid; border-left-width:0.75pt; border-right-color:#000000; border-right-style:solid; border-right-width:0.75pt; border-top-color:#000000; border-top-style:solid; border-top-width:0.75pt; padding-left:5.03pt; padding-right:5.03pt; vertical-align:top; width:137.3pt">
            <p style="margin-bottom:0pt; text-align:center; line-height:normal; font-size:12pt">
              <span style="font-family:Calibri; font-size:12pt">{{$avg->average}}</span>
            </p>
          </td>
        </tr>
        @endforeach
        </tbody>
      </table>
      <p style="text-align:center; line-height:108%; font-size:12pt">
        <strong><span style="font-size:12pt; ">&#xa0;</span></strong>
      </p>
    </div>
  </body>
</html>
