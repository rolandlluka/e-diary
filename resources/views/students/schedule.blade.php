@extends('layouts.app')

@section('content')
<section class="content-header">
      <h1>
        Ketu mund te shihni orarin per te gjithe javen ne vazhdim
      </h1>
      <ol class="breadcrumb">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
      </ol>
</section>
<section class="content">

<div class="box-body">
          <div class="box box-primary">
            <div class="box-body no-padding">
              <!-- THE CALENDAR -->
              <div id="calendar"></div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /. box -->
        </div>
</section>
@endsection

@section('scripts')
<script>

    var repeatingEvents = [
 @foreach($schedules as $entry)
      {
        title:"{{$entry->subject->name}} - {{$entry->professor->name}} {{$entry->professor->lastname}}",
        id: {{$entry->id}},
        start: "{{$entry->koha_fillimit}}", // a start time (10am in this example)
        end: "{{$entry->koha_mbarimit}}", // an end time (6pm in this example)
        dow: [{{$entry->dita}}], // Repeat monday and thursday
        ranges: [{ //repeating events are only displayed if they are within one of the following ranges.
            start: moment().startOf('week'), //next two weeks
            end: moment().endOf('week').add(5,'d'),
        },
        {
            start: moment('2016-02-01','YYYY-MM-DD'), //all of february
            end: moment('2017-09-01','YYYY-MM-DD').endOf('month'),
        }
        ]
      },
      @endforeach
    ];
    var getEvents = function( start, end ){
    return repeatingEvents;
}
$('#calendar').fullCalendar({
    defaultDate: moment(),
    slotDuration: '00:15:00',
    weekends: false,
    header: {
        left: 'prev,next today',
        center: 'title',
        right: 'agendaWeek,month,agendaDay'
    },
    defaultView: 'agendaWeek',
    eventRender: function(event, element, view){
        console.log(event.start.format());
        return (event.ranges.filter(function(range){
            return (event.start.isBefore(range.end) &&
                    event.end.isAfter(range.start));
        }).length)>0;
    },
    events: function( start, end, timezone, callback ){
        var events = getEvents(start,end); //this should be a JSON request
        
        callback(events);
    },
});  
          


  /*var repeatingEvents = [
  {
    title:"My repeating event",
    id: 1,
    start: '10:00', // a start time (10am in this example)
    end: '12:00', // an end time (6pm in this example)
    dow: [ 1], // Repeat monday and thursday
    ranges: [{ //repeating events are only displayed if they are within one of the following ranges.
        start: moment().startOf('week'), //next two weeks
        end: moment().endOf('week').add(5,'d'),
    },
    {
        start: moment('2016-02-01','YYYY-MM-DD'), //all of february
        end: moment('2100-02-01','YYYY-MM-DD').endOf('month'),
    }
    ],
  }
];*/

//emulate server


</script>
@endsection

