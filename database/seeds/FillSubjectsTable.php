<?php

use Illuminate\Database\Seeder;
use App\Subjects;
class FillSubjectsTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subjects = [
            'Matematike',
            'Biologji',
            'Fizike',
            'Kimi',
            'Gjuhe Shqipe',
            'Gjuhe Angleze',
            'Gjuhe Franceze',
            'Art figurativ',
            'Edukate Fizike',
            'Informatike',
        ];

        foreach ($subjects as $subject)
        {
            Subjects::create([
                'name' => $subject,
            ]);
        }
    }
}
