<?php

use Illuminate\Database\Seeder;
use App\User;

class FillUsersTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = new User([
            'name' => 'admin',
            'lastname' => 'admin',
            'email' => 'administrator@gmail.com',
            'password' => '$2y$10$EpXqMZhC.tEXafTyLTS6mOXgjsR9hA0YNqTUcvV8SB.3LZvNdHveO',
            'role_id' => 1
        ]);
        $users->save();
    }
}
