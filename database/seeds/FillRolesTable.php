<?php

use Illuminate\Database\Seeder;

use App\Roles;

class FillRolesTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = array(

            array(
                'description' => 'Administrator'
            ),
            array(
                'description' => 'Nxenes'
            ),
            array(
                'description' => 'Profesor'
            ),
            array(
                'description' => 'Prind'
            )
        );

        Roles::insert($roles);
    }
}
