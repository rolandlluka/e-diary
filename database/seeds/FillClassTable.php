<?php

use Illuminate\Database\Seeder;

use App\Clas;

class FillClassTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $class = array(

            array(
                'class' => 10,
                'parallel' => 1,
                'status' => 1,
            ),
            array(
                'class' => 11,
                'parallel' => 1,
                'status' => 1,
            ),
            array(
                'class' => 12,
                'parallel' => 1,
                'status' => 1,
            ),
            array(
                'class' => 10,
                'parallel' => 2,
                'status' => 1,
            ),
            array(
                'class' => 11,
                'parallel' => 2,
                'status' => 1,
            ),
            array(
                'class' => 12,
                'parallel' => 2,
                'status' => 1,
            ),
            array(
                'class' => 10,
                'parallel' => 3,
                'status' => 1,
            ),
            array(
                'class' => 11,
                'parallel' => 3,
                'status' => 1,
            ),
            array(
                'class' => 12,
                'parallel' => 3,
                'status' => 1,
            ),
            array(
                'class' => 10,
                'parallel' => 4,
                'status' => 1,
            ),
            array(
                'class' => 11,
                'parallel' => 4,
                'status' => 1,
            ),
            array(
                'class' => 12,
                'parallel' => 4,
                'status' => 1,
            ),
            array(
                'class' => 10,
                'parallel' => 5,
                'status' => 1,
            ),
            array(
                'class' => 11,
                'parallel' => 5,
                'status' => 1,
            ),
            array(
                'class' => 12,
                'parallel' => 5,
                'status' => 1,
            ),
            array(
                'class' => 10,
                'parallel' => 6,
                'status' => 1,
            ),
            array(
                'class' => 11,
                'parallel' => 6,
                'status' => 1,
            ),
            array(
                'class' => 12,
                'parallel' => 6,
                'status' => 1,
            ),
            array(
                'class' => 10,
                'parallel' => 7,
                'status' => 1,
            ),
            array(
                'class' => 11,
                'parallel' => 7,
                'status' => 1,
            ),
            array(
                'class' => 12,
                'parallel' => 7,
                'status' => 1,
            ),
            array(
                'class' => 10,
                'parallel' => 8,
                'status' => 1,
            ),
            array(
                'class' => 11,
                'parallel' => 8,
                'status' => 1,
            ),
            array(
                'class' => 12,
                'parallel' => 8,
                'status' => 1,
            ),
            array(
                'class' => 10,
                'parallel' => 9,
                'status' => 1,
            ),
            array(
                'class' => 11,
                'parallel' => 9,
                'status' => 1,
            ),
            array(
                'class' => 12,
                'parallel' => 9,
                'status' => 1,
            ),
            array(
                'class' => 10,
                'parallel' => 10,
                'status' => 1,
            ),
            array(
                'class' => 11,
                'parallel' => 10,
                'status' => 1,
            ),
            array(
                'class' => 12,
                'parallel' => 10,
                'status' => 1,
            ),
            array(
                'class' => 10,
                'parallel' => 11,
                'status' => 1,
            ),
            array(
                'class' => 11,
                'parallel' => 11,
                'status' => 1,
            ),
            array(
                'class' => 12,
                'parallel' => 11,
                'status' => 1,
            ),
            array(
                'class' => 10,
                'parallel' => 12,
                'status' => 1,
            ),
            array(
                'class' => 11,
                'parallel' => 12,
                'status' => 1,
            ),
            array(
                'class' => 12,
                'parallel' => 12,
                'status' => 1,
            ),
        );

        Clas::insert($class);
    }
}
