<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function(Blueprint $table) {

            $table->increments('id');
            $table->string('name');
            $table->string('lastname');
            $table->string('birthplace');
            $table->integer('academic_id');
            $table->string('place');
            $table->date('birthday');
            $table->integer('gender');
            $table->integer('status');
            $table->integer('parent_id')->unsigned();
            $table->integer('clas_id')->unsigned();
            $table->timestamps();

            $table->foreign('parent_id')->references('id')->on('parents')->onDelete('cascade');
            $table->foreign('clas_id')->references('id')->on('clas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('students');
    }
}
