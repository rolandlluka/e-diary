<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subject_id');
            $table->integer('clas_id');
            $table->integer('professor_id');
            $table->timestamp('ora');
            $table->string('pershkrimi');
            $table->integer('semester');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('programi');
    }
}
