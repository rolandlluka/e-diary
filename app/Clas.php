<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clas extends Model
{
    protected $fillable = ['class', 'parallel'];



    public function professor(){

        return $this->belongsToMany('App\Professor', 'professors_clas', 'professor_id', 'clas_id');
    }

    public function schedule(){

        return $this->hasMany('App\Shcedule');
    }
    public function student(){

        return $this->hasMany('App\Student');
    }
    public function mark(){

        return $this->hasMany('App\Mark');
    }
    public function absence(){

        return $this->hasMany('App\Absence');
    }
    public function programi(){

        return $this->hasMany('App\Programi');
    }
}
