<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Absence extends Model
{
	public $timestamps = false;
    public function student(){

    	return $this->belongsTo('App\Students');
    }
    public function clas(){

    	return $this->belongsTo('App\Clas');
    }
    public function subject(){

    	return $this->belongsTo('App\Subjects');
    }
}
