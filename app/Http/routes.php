<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



    Route::get('/', [
    'uses' => 'UserController@getLogin',
    'as' => 'login'
    ]);
    Route::post('/login', [
            'uses' => 'UserController@postLogin',
            'as' => 'user.login'
        ]);
    Route::get('/home', 'HomeController@index');
    Route::get('logout', [
            'uses' => 'UserController@getLogout',
            'as' => 'user.logout'
        ]);

   
Route::group(['middleware' => 'auth','prefix' => 'admin', 'namespace' => 'Administrator'], function () {

 
    
    
    Route::post('/newNotice', ['uses' => 'UserController@newNotice','as' => 'new.notice' ]);
    Route::get('/getUpdate', ['uses' => 'UserController@getUpdate', 'as' => 'get.update']);
    Route::put('/newNotice', ['uses' => 'UserController@newUpdate', 'as' => 'new.notice']);
    Route::post('/deleteNotice', ['uses' => 'UserController@deleteNotice', 'as' => 'del.notice' ]);
    Route::get('/profile/{id}', ['as' => 'userProfile', 'uses' => 'UserController@getProfile']);
    Route::post('/change/{id}', ['as' => 'changeProfile', 'uses' => 'UserController@changeProfile']);
    Route::post('/image/{id}', ['as' => 'uploadAvatar', 'uses' => 'UserController@changeAvatar']);
    
    Route::group(['middleware' => 'admin', 'prefix' => 'users'], function () {
        
        Route::get('/dashboard', ['as' => 'adminDashboard', 'uses' => 'UserController@dashboard']);
        Route::get('/', ['as' => 'homeUser', 'uses' => 'UserController@index']);
        Route::get('/create', ['as' => 'createUser', 'uses' => 'UserController@create']);
        Route::get('/{id}', ['as' => 'singleUser', 'uses' => 'UserController@single']);

        Route::post('/store', ['as' => 'storeUser', 'uses' => 'UserController@store']);
        Route::post('/update/{id}', ['as' => 'updateUser', 'uses' => 'UserController@update']);
        Route::post('/delete/{id}', ['as' => 'deleteUser', 'uses' => 'UserController@delete']);

    });

    Route::group(['middleware' => 'admin','prefix' => 'professor'], function () {

        Route::get('/programi', ['as' => 'getProgram', 'uses' => 'ProfessorController@programindex']);
        Route::get('/programi-ajax/{professor_id}/{clas_id}/{date1}/{date2}', ['as' => 'getProgrami', 'uses' => 'ProfessorController@getProgram']);
        Route::get('/', ['as' => 'homeProfessor', 'uses' => 'ProfessorController@index']);
        Route::get('/create', ['as' => 'createProfessor', 'uses' => 'ProfessorController@create']);
        Route::get('/{id}', ['as' => 'editProfessor', 'uses' => 'ProfessorController@edit']);

        Route::post('/store', ['as' => 'storeProfessor', 'uses' => 'ProfessorController@store']);
        Route::post('/update/{id}', ['as' => 'updateProfessor', 'uses' => 'ProfessorController@update']);
        Route::post('/delete/{id}', ['as' => 'deleteProfessors', 'uses' => 'ProfessorController@destroy']);
        Route::post('/passiveProfessor/{id}', ['as' => 'passiveProfessor', 'uses' => 'ProfessorController@passive']);
        Route::post('/activeProfessor/{id}', ['as' => 'activeProfessor', 'uses' => 'ProfessorController@active']);

    });

    Route::group(['middleware' => 'admin','prefix' => 'subjects'], function () {
        Route::get('/', ['as' => 'homeSubjects', 'uses' => 'SubjectsController@index']);
        Route::get('/create', ['as' => 'createSubjects', 'uses' => 'SubjectsController@create']);
        Route::get('/{id}', ['as' => 'editSubjects', 'uses' => 'SubjectsController@edit']);

        Route::post('/delete/{id}', ['as' => 'deleteSubjects', 'uses' => 'SubjectsController@destroy']);
        Route::post('/update/{id}', ['as' => 'updateSubjects', 'uses' => 'SubjectsController@update']);
        Route::post('/active/{id}', ['as' => 'activeSubjects', 'uses' => 'SubjectsController@active']);
        Route::post('/passive/{id}', ['as' => 'passiveSubjects', 'uses' => 'SubjectsController@passive']);
        Route::post('/store', ['as' => 'storeSubjects', 'uses' => 'SubjectsController@store']);
    });

    Route::group(['middleware' => 'admin','prefix' => 'students'], function () {
        Route::get('/increaseyear', ['as' => 'increaseyear', 'uses' => 'StudentsController@increaseyear']);
        Route::get('/', ['as' => 'homeStudents', 'uses' => 'StudentsController@index']);
        Route::get('/create', ['as' => 'createStudents', 'uses' => 'StudentsController@create']);
        Route::get('/{id}', ['as' => 'editStudents', 'uses' => 'StudentsController@edit']);

        
        Route::post('/store', ['as' => 'storeStudents', 'uses' => 'StudentsController@store']);
        Route::post('/update/{id}', ['as' => 'updateStudents', 'uses' => 'StudentsController@update']);
        Route::post('/delete/{id}', ['as' => 'deleteStudents', 'uses' => 'StudentsController@destroy']);
        Route::post('/passiveStudents/{id}', ['as' => 'passiveStudents', 'uses' => 'StudentsController@passive']);
        Route::post('/activeStudents/{id}', ['as' => 'activeStudents', 'uses' => 'StudentsController@active']);


    });

    Route::group(['middleware' => 'admin','prefix' => 'parents'], function () {

        Route::get('/', ['as' => 'homeParents', 'uses' => 'ParentsController@index']);
        Route::get('/create', ['as' => 'createParents', 'uses' => 'ParentsController@create']);  
        Route::get('/{id}', ['as' => 'editParents', 'uses' => 'ParentsController@edit']);

        Route::post('/store', ['as' => 'storeParents', 'uses' => 'ParentsController@store']);
        Route::post('/update/{id}', ['as' => 'updateParents', 'uses' => 'ParentsController@update']);
        Route::post('/delete/{id}', ['as' => 'deleteParents', 'uses' => 'ParentsController@destroy']);
        Route::post('/passiveParents/{id}', ['as' => 'passiveParents', 'uses' => 'ParentsController@passive']);
        Route::post('/activeParents/{id}', ['as' => 'activeParents', 'uses' => 'ParentsController@active']);


    });

    Route::group(['middleware' => 'admin','prefix' => 'class'], function () {

        Route::get('/', ['as' => 'homeClass', 'uses' => 'ClassController@index']);
        Route::get('/create', ['as' => 'createClass', 'uses' => 'ClassController@create']);
        Route::get('/{id}', ['as' => 'editClass', 'uses' => 'ClassController@edit']);
//
        Route::post('/store', ['as' => 'storeClass', 'uses' => 'ClassController@store']);
        Route::post('/update/{id}', ['as' => 'updateClass', 'uses' => 'ClassController@update']);
        Route::post('/passiveParents/{id}', ['as' => 'passiveClass', 'uses' => 'ClassController@passive']);
        Route::post('/delete/{id}', ['as' => 'deleteClass', 'uses' => 'ClassController@destroy']);
        Route::post('/activeParents/{id}', ['as' => 'activeClass', 'uses' => 'ClassController@active']);
    });
     Route::group(['middleware' => 'admin','prefix' => 'schedules'], function () {
        Route::get('/ajax-professor', ['as' => 'ajaxProfessor', 'uses' => 'ScheduleController@ajaxProfessor']);
        Route::get('/', ['as' => 'homeSchedules', 'uses' => 'ScheduleController@index']);
        Route::get('/create', ['as' => 'createSchedules', 'uses' => 'ScheduleController@create']);
        Route::get('/{id}', ['as' => 'editSchedules', 'uses' => 'ScheduleController@edit']);

        Route::post('/update/{id}', ['as' => 'updateSchedules', 'uses' => 'ScheduleController@update']);
        Route::post('/delete/{id}', ['as' => 'deleteSchedules', 'uses' => 'ScheduleController@destroy']);
        Route::post('/store', ['as' => 'storeSchedule', 'uses' => 'ScheduleController@store']);
    });


});

    Route::group(['middleware' => 'student', 'prefix' => 'student', 'namespace' => 'Student'], function () {

        Route::get('/dashboard', ['as' => 'studentDashboard', 'uses' => 'StudentController@dashboard']);

        Route::group(['prefix' => 'schedules'], function () {

            Route::get('calendar/{id}', ['as' => 'homeSchedule', 'uses' => 'StudentController@index']);
        });
    }); 

     Route::group(['middleware' => 'student','prefix' => 'student', 'namespace' => 'Administrator'], function () {

        Route::group(['prefix' => 'marks'], function () {
            Route::get('addmarks/{id}', ['as' => 'addprofessormarks', 'uses' => 'MarksController@professorMarks']);
            Route::get('/{id}', ['as' => 'homeMark', 'uses' => 'MarksController@index']);
            Route::get('/absence/{id}', ['as' => 'homeAbsence', 'uses' => 'MarksController@absence']);
            Route::get('/printabsence/{id}', ['uses'=>'AbsenceController@mungesaPrint', 'as'=> 'raport.absence']);
            Route::get('/printmarks/{id}', ['uses'=>'MarksController@marksPrint', 'as'=> 'raport.marks']);
        });    


    });

     Route::group(['middleware' => 'parent','prefix' => 'parent', 'namespace' => 'Parent'], function () {

        Route::get('/dashboard', ['as' => 'parentDashboard', 'uses' => 'ParentController@dashboard']);
        Route::group(['prefix' => 'marks'], function () {
            Route::get('/absences', ['as' => 'getchildren', 'uses' => 'ParentController@getChildren']);
            Route::get('/absences/{id}', ['as' => 'getbyabsence', 'uses' => 'ParentController@absences']);
            Route::get('/', ['as' => 'parentStudents', 'uses' => 'ParentController@getMark']);
            Route::get('/{id}', ['as' => 'studentMark', 'uses' => 'ParentController@getStudent']);
            Route::get('/printabsence/{id}', ['uses'=>'ParentController@mungesaPrint', 'as'=> 'printabsence']);
            Route::get('/printmarks/{id}', ['uses'=>'ParentController@marksPrint', 'as'=> 'printmarks']);
        });    


    });
    Route::group(['middleware' => 'professor','prefix' => 'professor', 'namespace' => 'Professor'], function () {

        Route::get('/dashboard', ['as' => 'professorDashboard', 'uses' => 'ProfessorController@dashboard']);
        Route::group(['prefix' => 'marks'], function () {
            Route::get('/', ['as' => 'professorMarks', 'uses' => 'ProfessorController@marks']);
            Route::get('/testmarks', ['as' => 'testMarks', 'uses' => 'ProfessorController@testMarks']);
            Route::get('/ajax-student', ['as' => 'ajaxStudent', 'uses' => 'ProfessorController@ajaxStudent']);
            Route::get('/ajax-marks/', ['as' => 'ajaxMarks', 'uses' => 'ProfessorController@ajaxMarks']);
            Route::get('/list', ['as' => 'addprofessormarks', 'uses' => 'ProfessorController@professorMarks']);
            Route::get('/getmark/{subject_id}/{student_id}', ['as' => 'getUpdateMarks', 'uses' => 'ProfessorController@getList']);
            Route::get('/edit', ['as' => 'editMarks', 'uses' => 'ProfessorController@editMarks']);
            Route::get('/absence', ['as' => 'absence', 'uses' => 'ProfessorController@absence']);
            Route::get('/editabsence', ['as' => 'editAbsence', 'uses' => 'ProfessorController@editAbsence']);
            Route::get('/getabsence/{id}', ['as' => 'getAbsence', 'uses' => 'ProfessorController@getAbsence']);
            Route::get('/list/absence', ['as' => 'professorAbsences', 'uses' => 'ProfessorController@professorAbsences']);
            Route::get('/ora', ['as' => 'oraMesimore', 'uses' => 'ProfessorController@programi']);
            Route::post('/storeMarks', ['as' => 'storeMarks', 'uses' => 'ProfessorController@storeMark']);
            Route::post('/storeTestmarks', ['as' => 'storeTestmarks', 'uses' => 'ProfessorController@storeTestmarks']);
            Route::post('/update/{id}', ['as' => 'updateMarks', 'uses' => 'ProfessorController@updateMarks']);
            Route::post('/storeAbsence', ['as' => 'storeAbsence', 'uses' => 'ProfessorController@storeAbsence']);
            Route::post('/updateabsence/{id}', ['as' => 'updateAbsence', 'uses' => 'ProfessorController@updateAbsence']);
            Route::post('/delete/{id}', ['as' => 'deleteAbsence', 'uses' => 'ProfessorController@destroy']);
            Route::post('/updateAbsence/{id}', ['as' => 'updateAbsence', 'uses' => 'ProfessorController@updateAbsence']);
            Route::post('/storeOra', ['as' => 'storeOra', 'uses' => 'ProfessorController@storeProgram']);

        });    


    });