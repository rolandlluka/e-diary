<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Students;
use App\Parents;
use App\AcademicYear;
use App\Schedule;
use App\Clas;
use App\Absence;
use App\Subjects;
use App\Professor;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;

class AbsenceController extends Controller
{
    public function index(){

        $class = Clas::all();
        
        return view('users/class/home', ['class' => $class]);

    }
    public function mungesaPrint($id){

        $students = Students::find($id);

        $absence = Absence::with('student','clas','subject')->where('student_id', $students->id)->get();
        
        $avg = Absence::select('absences.*', \DB::raw('Count(case when absences.arsye =1 then `id` end) As arsyeshme , Count(case when absences.arsye=0 then `id` end) As paarsyeshme'))
        ->groupBy('absences.subject_id')
        ->where('student_id', $students->id)
        ->get();
        
        $pdf = PDF::loadView('students.printabsence', compact('absence','avg'));

        return $pdf->stream(Carbon::now().'raport-mungesa.pdf');
    }
    
}
