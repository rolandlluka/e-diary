<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Clas;
use App\Professor;
use App\Professors_Clas;
use Flash;

class ClassController extends Controller
{
    
    public function index(){

        
        $class = Clas::paginate(15);
        return view('users/class/home', ['class' => $class]);
    }

    public function create()
    {

        $professors = Professor::all();

        return view('users.class.create', compact('professors'));
    }
    
    public function store(Request $request)
    {

        $this->validate($request, [
            'class' => 'required',
            'parallel' => 'required',
            'class_administrator' => 'required',
        ]);

        $class = new Clas();

        $class->class = $request->class;
        $class->parallel = $request->parallel;
        //$class->class_administrator = $request->class_administrator;
        $class->save();

        $class->professor()->attach($request->get('class_administrator'));

        Flash::info('Klasa u shtua me sukses');
        return redirect('/admin/class');
    }

    public function edit($id)
    {
        $class = Clas::find($id);
        $professors = Professor::all();

        return view('users.class.update', compact('class', 'professors'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'class' => 'required',
            'parallel' => 'required',
            'class_administrator' => 'required',
//

        ]);

        $class = Clas::find($id);

        $class->class = $request->class;
        $class->parallel = $request->parallel;
        $class->save();

        $class->professor()->detach();
        $class->professor()->attach($request->class_administrator);

        Flash::info('Klasa u ndryshua me sukses');
        return redirect('/admin/class');
    }

    public function destroy($id)
    {
        $clas = Clas::destroy($id);

        if($clas == true){

        Flash::warning('Klasa eshte fshire me sukses');

        return redirect()->back();
        }
    }
    public function passive($id)
    {
        $status = 0;
        $class = Clas::find($id);

        $class->status = $status;

        $class->save();

        if ($class == true) {

            return redirect()->back();
        }
    }

    public function active($id)
    {

        $status = 1;
        $class = Clas::find($id);

        $class->status = $status;

        $class->save();

        if ($class == true) {

            return redirect()->back();
        }
    }
}