<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Subjects;
use App\Professor;
use Illuminate\Support\Facades\Redirect;
use Flash;


class SubjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subjects = Subjects::paginate(5);
//
//        return view('users.professor.home', ['professors' => $professors]);
        return view('users.subjects.subjects', ['subjects' => $subjects]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $professors = Professor::all();

        return view('users.subjects.create', compact('professors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',

        ]);
        $subjects = new Subjects();

        $subjects->name = $request->name;

        $subjects->save();

        Flash::info('Lenda u ruajt me sukses');
        return redirect('/admin/subjects');
    }

    public function edit($id){
        $subjects = Subjects::find($id);
        $professors = Professor::all();


        return view('users.subjects.update', compact('subjects','professors'));
    }

    public function update(Request $request, $id)
    {
        

        $this->validate($request, [
            'name' => 'required'
        ]);


        $subjects = Subjects::find($id);

        $subjects->name = $request->name;

        $subjects->save();

        Flash::info('Lenda eshte ndryshuar me sukses');
        return redirect('/admin/subjects');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subjects = Subjects::destroy($id);

        if ($subjects == true) {

            Flash::warning('Lenda eshte fshire me sukses');
            return redirect()->back();
        }
    }

    public function passive($id)
    {
        $status = 0;
        $subjects = Subjects::find($id);

        $subjects->status = $status;

        $subjects->save();

        if ($subjects == true) {

            return redirect()->back();
        }
    }

    public function active($id)
    {

        $status = 1;
        $subjects = Subjects::find($id);

        $subjects->status = $status;

        $subjects->save();

        if ($subjects == true) {

            return redirect()->back();
        }
    }

}
