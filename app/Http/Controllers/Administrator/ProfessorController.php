<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Professor;
use App\Subjects;
use App\User;
use App\Programi;
use App\Clas;
use Flash;
class ProfessorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $professors = Professor::paginate(10);

        return view('users.professor.home', ['professors' => $professors]);
    }

    public function create()
    {
        $subjects = Subjects::all();

        return view('users.professor.create', compact('subjects'));
    }

    public function store(Request $request)
    {
//        return $request->all();

        $this->validate($request, [
            'name' => 'required|min:3|max:255',
            'lastname' => 'required|min:3|max:255',
            'gender' => 'required',
            'birthday' => 'required',
            'email' => 'required|max:150|unique:users',
        ]);

        $professor = new Professor();
        $status = 0;

        $professor->name = $request->name;
        $professor->lastname = $request->lastname;
        $professor->gender = $request->gender;
        $professor->birthday = $request->birthday;
        $professor->status = $status;

        $professor->save();

        $user = new User;
        $user->name = $request->name;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->professor_id = $professor->id;
        $user->role_id = 3;
        $user->save();


        foreach($request->get('subject') as $key => $sub)
        {
            $professor->subjects()->attach($key);
        }
        

        return redirect('/admin/professor');

    }

    public function edit($id)
    {
        $professor = Professor::with('subjects')->find($id);
        $subjects = Subjects::all();
        $hasSubjects = [];

        foreach($professor->subjects as $subject)
        {
            $hasSubjects[] = $subject->id;
        }

        return view('users.professor.update', ['professor' => $professor, 'subjects' => $subjects, 'hasSubjects' => $hasSubjects]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|min:3|max:255',
            'lastname' => 'required|min:3|max:255',
            'gender' => 'required',
            'birthday' => 'required',
        ]);

        $professor = Professor::find($id);

        $professor->name = $request->name;
        $professor->lastname = $request->lastname;
        $professor->gender = $request->gender;
        $professor->birthday = $request->birthday;

        $professor->save();
        $professor->subjects()->detach();
        
        foreach($request->get('subject') as $key => $sub)
        {
            $professor->subjects()->attach($key);
        }

//        return redirect()->back();
        return redirect('/admin/professor');

    }

    public function passive($id)
    {
        $status = 0;
        $professor = Professor::find($id);

        $professor->status = $status;

        $professor->save();

        if ($professor == true){

            return redirect()->back();
        }
    }

    public function active($id){

        $status = 1;
        $professor = Professor::find($id);

        $professor->status = $status;

        $professor->save();

        if($professor == true){

            return redirect()->back();
        }
    }

    public function destroy($id)
    {
        $professor = Professor::destroy($id);

        if($professor == true){

        Flash::warning('Profesori eshte fshire me sukses');

        return redirect()->back();
        }
    }

    public function programindex(){

        $class = Clas::all();
        $professor = Professor::all();

        return view('users.professor.program', compact('class','professor'));
    }

    public function getProgram($professor_id, $clas_id, $date1, $date2){

            $program = Programi::with('clas','subject','professor')->where('professor_id', $professor_id)->where('clas_id', $clas_id)->whereBetween('ora', [$date1, $date2])->get();
            return response()->json($program);
    }
}
