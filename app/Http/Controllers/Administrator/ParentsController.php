<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Parents;
use App\User;
use Flash;


class ParentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $parents = Parents::all();

        return view('users/parents/home', ['parents' => $parents]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.parents.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'lastname' => 'required',
            'gender' => 'required',
            'birthday' => 'required',
            'birthplace' => 'required',
            'email' => 'required|max:150|unique:users'
        ]);

        $parents = new Parents();
        $status = 0;

        $parents->name = $request->name;
        $parents->lastname = $request->lastname;
        $parents->gender = $request->gender;
        $parents->birthday = $request->birthday;
        $parents->birthplace = $request->birthplace;
        $parents->status = $status;

        $parents->save();


        $user = new User;
        $user->name = $request->name;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->parent_id = $parents->id;
        $user->role_id = 4;
        $user->save();

        Flash::warning('Prindi u shtua me sukses');
        return redirect('/admin/parents');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $parents = Parents::find($id);

        return view('users.parents.update', ['parents' => $parents]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'lastname' => 'required',
            'gender' => 'required',
            'birthday' => 'required',
            'birthplace' => 'required'
        ]);

        $parents = Parents::find($id);

        $parents->name = $request->name;
        $parents->lastname = $request->lastname;
        $parents->gender = $request->gender;
        $parents->birthday = $request->birthday;
        $parents->birthplace = $request->birthplace;

        $parents->save();

        Flash::warning('Prindi u ndryshua me sukses');
        return redirect('/admin/parents');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $parent = Parents::destroy($id);

        if($parent == true){

        Flash::warning('Prindi u fshie me sukses');

        return redirect()->back();
        }
    }

    public function passive($id)
    {
        $status = 0;
        $parents = Parents::find($id);

        $parents->status = $status;

        $parents->save();

        if ($parents == true) {

            return redirect()->back();
        }
    }

    public function active($id)
    {

        $status = 1;
        $parents = Parents::find($id);

        $parents->status = $status;

        $parents->save();

        if ($parents == true) {

            return redirect()->back();
        }
    }
}
