<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Students;
use App\Parents;
use App\User;
use App\AcademicYear;
use App\Clas;
use Flash;

class StudentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Students::with('parent','academic','clas')->paginate(10);
        
        return view('users/students/home', compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()

    {
        $parents = Parents::all();
        $academic = AcademicYear::all();
        $clas = Clas::all();
        return view('users/students/create', compact('parents','academic','clas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [

            'name' => 'required|max:255|min:3',
            'birthplace' => 'required|max:255|min:3',
            'place' => 'required|max:255|min:3',
            'birthday' => 'required|max:255|min:3',
            'gender' => 'required',
            'email' => 'required|max:150|unique:users',

        ]);

        $students = new Students;
        $status = 0;

        $students->name = $request->name;
        $students->lastname = $request->lastname;
        $students->parent_id = $request->parent;
        $students->academic_id = $request->academic;
        $students->birthplace = $request->birthplace;
        $students->place = $request->place;
        $students->birthday = $request->birthday;
        $students->parent_id = $request->parent;
        $students->clas_id = $request->clas;
        $students->gender = $request->gender;
        $students->save();


        $user = new User;
        $user->name = $request->name;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->student_id = $students->id;
        $user->role_id = 2;
        $user->save();

        Flash::info('Nxenesi eshte shtuar  me sukses');
        return redirect('/admin/students');
    }

    public function edit($id)
    {
        $students = Students::find($id);
        $parents = Parents::all();
        return view('users.students.update', ['students' => $students, 'parents' => $parents]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'name' => 'required',
            'parentName' => 'required',
            'birthplace' => 'required',
            'place' => 'required',
            'birthday' => 'required',
            'gender' => 'required',


        ]);
        $students = Students::find($id);

        $students->name = $request->name;
        $students->parent_id = $request->parentName;
        $students->birthplace = $request->birthplace;
        $students->place = $request->place;
        $students->birthday = $request->birthday;
        $students->gender = $request->gender;

        $students->save();

        Flash::info('Nxenesi eshte ndryshuar  me sukses');
        return redirect('/admin/students');

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $students = Students::destroy($id);

        if($students == true){

        Flash::warning('Nxenesi eshte fshire me sukses');

        return redirect()->back();
        }
    }

    public function passive($id)
    {
        $status = 0;
        $students = Students::find($id);

        $students->status = $status;

        $students->save();

        if ($students == true) {

            return redirect()->back();
        }
    }

    public function active($id){

        $status = 1;
        $students = Students::find($id);

        $students->status = $status;

        $students->save();

        if ($students == true) {

            return redirect()->back();
        }
    }

    public function increaseyear(){ 

        $students = Students::whereHas('clas', function ($q)
                {
                   $q->where('class', '!=', 13);
                })
        ->where('status', '=', 0)->get();

        foreach ($students as $stud) {
            
            $update = Clas::where('class', $stud->clas->class+1)->where('parallel', $stud->clas->parallel)->first();

                $student = Students::find($stud->id);

                $student->clas_id = $update->id;
                $student->save();
         }
    }
}
