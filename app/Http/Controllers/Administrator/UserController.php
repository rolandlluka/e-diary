<?php

namespace App\Http\Controllers\Administrator;

use App\Roles;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Students;
use App\Professor;
use App\Parents;
use App\Absence;
use App\Notice;
use App\Mark;
use Flash;
use Carbon\Carbon;
use Hash;
use Auth;
use Image;
use File;

class UserController extends Controller
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function dashboard()
    {

         $avg = Mark::select('marks.*', \DB::raw('avg(marks.mark) AS average'))
        ->groupBy('marks.clas_id')
        ->orderBy('average', 'asc')
        ->take(4)
        ->get();


        $students = Students::where('status', 1)->count('created_at');

        $professors = Professor::where('status', 1)->count('created_at');

        $parents = Parents::where('status', 1)->count('created_at');

        $absences = Absence::whereDate('koha', '=', Carbon::today()->toDateString())->count('koha');

        $notices = Notice::whereBetween('koha_fillimit',  [ Carbon::parse('january')->startOfMonth(), Carbon::parse('june')->endOfMonth()])->get();


        return view('users.dashboard', compact('students', 'professors', 'parents', 'absences', 'notices', 'avg'));
    }

    public function index()
    {
        $users = User::all();

        return view('users.home', compact('users'));
    }

    public function create()
    {
        $roles = Roles::all();
        return view('users.create', compact('roles'));
    }

    /**
     * @param Request $request
     */
    public function store(Request $request){

        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|max:150|unique:users',
        ]);

        $user = new User;

        $user->name = $request->name;
        $user->email = $request->email;
        $user->role_id = 1;
        $user->password = bcrypt($request->password);

        $user->save();

        Flash::info('Perdoruesi u shtua me sukses');
        return redirect()->back();
    }

    public function single($id)
    {
        $roles = Roles::all();
        $user = User::with('role')->find($id);

        return view('users.update', compact('roles', 'user'));
    }

    public function update($id)
    {
        $this->validate($this->request, [
            'name' => 'required|string|min:3|max:255',
            'email' => 'required|email',
            'password_confirmation' => 'same:newpassword',
        ]);

        // Find user
        $user = User::find($id);

        $user->name = $this->request->get('name');
        $user->email = $this->request->get('email');
        $user->role_id = (int) $this->request->get('role_id');
        $user->password = bcrypt($this->request->newpassword);
        $user->save();

        Flash::info('Perdoruesi u ndryshua me sukses');
        return redirect()->back();
    }

    public function delete($id)
    {
        $user = User::destroy($id);

        if($user == true){
            Flash::warning('Perdoruesi eshte fshire me sukses');
            return redirect()->back();
        }
    }

    public function newNotice(Request $request){
        if($request->ajax()){
            $notice = Notice::create($request->all());
            return response()->json($notice);
        }
    }

    public function getUpdate(Request $request){
        if ($request->ajax()) {
            $notice = Notice::find($request->id);
            return Response($notice);
        }
    }
    public function newUpdate(Request $request){
        if($request->ajax()){

            $notice = Notice::find($request->id);
            $notice->pershkrimi=$request->pershkrimi;
            $notice->role = $request->role;
            $notice->koha_fillimit = $request->koha_fillimit;
            $notice->koha_mbarimit = $request->koha_mbarimit;
            $notice->save();
            return Response($notice);
        }
    }
    public function deleteNotice(Request $request){
        if($request->ajax()){
            Notice::destroy($request->id);
            return Response()->json(['sms'=> 'U fshi me sukses']);
        }
    }
     public function getProfile($id)
    {
        
        $user = User::find(Auth::user()->id);

        
        return view('users.profile', compact('user'));
    }

     public function changeProfile(Request $request, $id) {

        $this->validate($this->request, [
            'newpassword' => 'required',
            'password_confirmation' => 'required|same:newpassword'
        ]);

        
        $newpassword = $request->oldpassword;
        $oldpassword = User::find(Auth::user()->id)->password;

        if(Hash::check($newpassword, $oldpassword)){
            $user = User::find(Auth::user()->id);
            $user->password = Hash::make($request->newpassword);
            $user->save();

            Auth::logout();

            Flash::info('Sapo ndryshuat passwordin!');
            return redirect('/');
        }else{

            Flash::info('Passwordi i vjeter eshte shkruar gabim!');
            return redirect()->back();
        }

    }

     public function changeAvatar(Request $request,$id){
        
        $user = User::find(Auth::user()->id);

        if ($request->hasFile('avatar')) {

            $avatar = $request->file('avatar');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();

            // Delete current image before uploading new image
            if ($user->avatar !== 'default.png') {
                
                $file = 'dist/avatars/' . $user->avatar;
                

                if (File::exists($file)) {
                    unlink($file);
                }

            }
            
            Image::make($avatar)->resize(300, 300)->save('dist/avatars/' . $filename);
            $user = Auth::user();
            $user->avatar = $filename;
            $user->save();
        }
            Flash::success('U ndryshua me sukses');
            return redirect()->back();

    }

}
