<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Students;
use App\Parents;
use App\AcademicYear;
use App\Schedule;
use App\Clas;
use App\Subjects;
use App\Professor;
use App\ProfessorsSubject;
use Flash;
use Illuminate\Support\Facades\Input;

class ScheduleController extends Controller
{
   public function index()
    {
        $schedules = Schedule::with('clas','subject','professor')->get();
        
        return view('users/students/schedule', compact('schedules'));
    } 

    public function create()

    {
        $class = Clas::all();
        $subjects = Subjects::all();
        return view('users/students/createSchedule', compact('class','subjects','professor'));
    }
    public function store(Request $request)
    {
        $this->validate($request, [

            'clas' => 'required',
            'professor' => 'required',
            'subject' => 'required',
            'dita' => 'required'

        ]);

        $schedules = new Schedule;

        $schedules->clas_id = $request->clas;
        $schedules->subject_id = $request->subject;
        $schedules->professor_id = $request->professor;
        $schedules->dita = $request->dita;
        $schedules->koha_fillimit = $request->koha_fillimit;
        $schedules->koha_mbarimit = $request->koha_mbarimit;
        $schedules->save();

        Flash::info('Orari u shtua me sukses');

        return redirect('/admin/schedules');
    }
     public function edit($id)
    {
        $schedules = Schedule::with('clas','subject','professor')->find($id);

        $subjects = Subjects::all();
        $professor = Professor::all();
        return view('users.students.updateSchedule', compact('schedules', 'subjects','professor'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'professor' => 'required',
            'subject' => 'required',
            'dita' => 'required'

        ]);

        $schedules = Schedule::find($id);

        $schedules->subject_id = $request->subject;
        $schedules->professor_id = $request->professor;
        $schedules->dita = $request->dita;
        $schedules->koha_fillimit = $request->koha_fillimit;
        $schedules->koha_mbarimit = $request->koha_mbarimit;
        $schedules->save();

        Flash::info('orari u ndryshua me sukses');
        return redirect('/admin/schedules');

    }
    public function destroy($id)
    {
        $schedules = Schedule::destroy($id);

        if($schedules == true){
            Flash::warning('Orari eshte fshire me sukses');
            return redirect()->back();
        }
    }

    public function ajaxProfessor(){

        $subject_id = Input::get('subject_id');

        $professors = ProfessorsSubject::with('professor')->select('professor_id')->where('subject_id', $subject_id)->get();

        return \Response::json($professors);
    }
}
