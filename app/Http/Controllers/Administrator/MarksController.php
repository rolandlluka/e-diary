<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Students;
use App\Parents;
use App\AcademicYear;
use App\Schedule;
use App\Clas;
use App\Mark;
use App\Absence;
use App\Subjects;
use App\Professor;
use Carbon\Carbon;
use Auth;
use Flash;
use Barryvdh\DomPDF\Facade as PDF;

class MarksController extends Controller
{
    public function index($id){

        if (  Auth::user()->student_id == $id) {
        

        $marks = Mark::with('student','clas','subject')->where('student_id', Auth::user()->student_id)->get();

        $avg = Mark::select('marks.*', \DB::raw('GROUP_CONCAT(marks.mark SEPARATOR " - ") as `notat`, avg(marks.mark) AS average'))
        ->groupBy('marks.subject_id')
        ->where('student_id', Auth::user()->student_id)
        ->get();

      
        return view('students.classmark', compact('marks','avg'));
       }else{

        
        return response()->view('errors.401');
       }

    }
   public function absence($id){

        if (  Auth::user()->student_id == $id) {
        

        $absence = Absence::with('student','clas','subject')->where('student_id', Auth::user()->student_id)->get();
        
        $avg = Absence::select('absences.*', \DB::raw('Count(case when absences.arsye =1 then `id` end) As arsyeshme , Count(case when absences.arsye=0 then `id` end) As paarsyeshme'))
        ->groupBy('absences.subject_id')
        ->where('student_id',  Auth::user()->student_id)
        ->get();
        
        return view('students.absence', compact('absence','avg'));

       }else{

        
        return response()->view('errors.401');
       }
    }

    public function marksPrint($id){

        $students = Students::find($id);

        $marks = Mark::with('student','clas','subject')->where('student_id', $students->id)->get();

        $avg = Mark::select('marks.*', \DB::raw('GROUP_CONCAT(marks.mark SEPARATOR " - ") as `notat`, avg(marks.mark) AS average'))
        ->groupBy('marks.subject_id')
        ->where('student_id', $students->id)
        ->get();
        
        $pdf = PDF::loadView('students.printmarks', compact('marks','avg'));

        return $pdf->stream(Carbon::now().'raport-marks.pdf');
    }
    
}
