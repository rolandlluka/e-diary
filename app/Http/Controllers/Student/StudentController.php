<?php

namespace App\Http\Controllers\Student;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Students;
use App\Parents;
use App\AcademicYear;
use App\Schedule;
use App\Clas;
use App\Subjects;
use App\Professor;
use App\Notice;
use App\Mark;
use App\Absence;
use Carbon\Carbon;
use Auth;
use Session;


class StudentController extends Controller
{

    public function index($id)
    {
    	$students = Students::find($id);

        $schedules = Schedule::with('subject','clas','professor')->where('clas_id', $students->clas_id)->get();
        
        return view('students/schedule', compact('schedules'));
    }
    public function dashboard()
    {
        $absences = Absence::whereDate('koha', '=', Carbon::today()->toDateString())->where('student_id', Auth::user()->student_id)->count('koha');

        $notices = Notice::whereBetween('koha_fillimit',  [ Carbon::parse('january')->startOfMonth(), Carbon::parse('june')->endOfMonth()])->where('role', '!=', 2)->get();

        $avg  = Mark::where('student_id', Auth::user()->student_id)->avg('mark');

        $pa = Absence::where('arsye', 0)->where('student_id', Auth::user()->student_id)->count('arsye');

        $mes = Mark::with('student')->where('student_id', Auth::user()->student_id)->where('semester',2)->groupBy('viti')->get();
       
        
        return view('students.dashboard', compact('notices','absences','avg', 'pa','mes'));
    }
}
