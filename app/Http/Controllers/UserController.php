<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Roles;
use App\User;
use App\Students;
use Auth;
use Session;
use Flash;
class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogin(){

        return view('users.authenticate.login');
    }

    public function postLogin(request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required|min:4'
        ]);

        $remember_me = false;

        if(isset($request->remember))
            $remember_me = true;


        if (Auth::attempt(['email'=>$request->input('email'), 'password'=>$request->input('password')], $remember_me)) {

            if (Auth::user()->role_id == 1) {
                return redirect()->route('adminDashboard');

            }elseif (Auth::user()->role_id == 2) {

                return redirect('/student/dashboard');
            }elseif (Auth::user()->role_id == 3) {
                return redirect('/professor/dashboard');
            }else

            return redirect('/parent/dashboard');
        }
        Flash::info('Emaili ose passwordi eshte gabim!');
        return redirect()->back()->withInput();
    } 

    public function getLogout(){
        
        Auth::logout();
        return redirect()->route('login');
    }
    public function index()
    {
        return view('home');
    }

    public function admin()
    {
        return view('admin.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    
}