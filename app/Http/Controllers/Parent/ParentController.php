<?php

namespace App\Http\Controllers\Parent;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Students;
use App\Parents;
use App\AcademicYear;
use App\Schedule;
use App\Clas;
use App\Mark;
use App\Notice;
use App\Subjects;
use App\Professor;
use App\Absence;
use App\User;
use Auth;
use Session;
use Carbon\Carbon;
use Barryvdh\DomPDF\Facade as PDF;


class ParentController extends Controller
{

    public function dashboard()
    {

        $child = Students::where('parent_id', '=', Auth::user()->parent_id)->count('parent_id');

        $notices = Notice::whereBetween('koha_fillimit',  [ Carbon::parse('january')->startOfMonth(), Carbon::parse('june')->endOfMonth()])->where('role', '!=', 2)->get();

        return view('parents.dashboard', compact('notices', 'child'));
    }
	public function getMark(){

		$students = Students::where('parent_id', '=', Auth::user()->parent_id)->get();

		return view('parents.students', ['students' => $students]);
	}

    public function getStudent($id){

        $student = Students::find($id);

        if (  $student->parent_id == Auth::user()->parent_id ) {
        

        $marks = Mark::with('student','clas','subject')->where('student_id', $student->id)->get();

        $avg = Mark::select('marks.*', \DB::raw('GROUP_CONCAT(marks.mark SEPARATOR " - ") as `notat`, avg(marks.mark) AS average'))
        ->groupBy('marks.subject_id')
        ->where('student_id', $student->id)
        ->get();
        
        return view('parents.classmarks', compact('marks', 'avg','student'));
       }else{

        return response()->view('errors.401');

        }
    }

    public function getChildren(){

		$students = Students::where('parent_id', '=', Auth::user()->parent_id)->get();

        
        
		return view('parents.studentabs', compact('students'));
	}
    public function absences($id){

        $students = Students::find($id);

        if (  $students->parent_id == Auth::user()->parent_id ) {
        

        $absence = Absence::with('student','clas','subject')->where('student_id', $students->id)->get();

        $avg = Absence::select('absences.*', \DB::raw('Count(case when absences.arsye =1 then `id` end) As arsyeshme , Count(case when absences.arsye=0 then `id` end) As paarsyeshme'))
        ->groupBy('absences.subject_id')
        ->where('student_id', $students->id)
        ->get();

        return view('parents.absence', compact('absence', 'avg', 'students'));
       }else{

        
        return response()->view('errors.401');
       }
    }

    public function marksPrint($id){

        $student = Students::find($id);

        $marks = Mark::with('student','clas','subject')->where('student_id', $student->id)->get();

        $avg = Mark::select('marks.*', \DB::raw('GROUP_CONCAT(marks.mark SEPARATOR " - ") as `notat`, avg(marks.mark) AS average'))
        ->groupBy('marks.subject_id')
        ->where('student_id', $student->id)
        ->get();
        
        
        $pdf = PDF::loadView('parents.printmarks', compact('marks', 'avg', 'student'));

        return $pdf->stream(Carbon::now().'raport-notat.pdf');
    }
    public function mungesaPrint($id){

        $students = Students::find($id);

        $absence = Absence::with('student','clas','subject')->where('student_id', $students->id)->get();

        $avg = Absence::select('absences.*', \DB::raw('Count(case when absences.arsye =1 then `id` end) As arsyeshme , Count(case when absences.arsye=0 then `id` end) As paarsyeshme'))
        ->groupBy('absences.subject_id')
        ->where('student_id', $students->id)
        ->get();
        
        $pdf = PDF::loadView('parents.printabsence', compact('absence', 'avg', 'students'));

        return $pdf->stream(Carbon::now().'raport-mungesa.pdf');
    }
}
