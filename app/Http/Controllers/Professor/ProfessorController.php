<?php

namespace App\Http\Controllers\Professor;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Students;
use App\Parents;
use App\ProfessorsSubject;
use App\ProfessorsClas;
use App\AcademicYear;
use App\Schedule;
use App\Clas;
use App\Mark;
use App\Subjects;
use App\Professor;
use App\Absence;
use App\Programi;
use App\Notice;
use Auth;
use Session;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Facades\Input;

class ProfessorController extends Controller
{
    public function dashboard()
    {
        $absences = Absence::whereDate('koha', '=', Carbon::today()->toDateString())->where('professor_id', Auth::user()->professor_id)->count('koha');
        
        $marks = Mark::whereDate('created_at', '=', Carbon::today()->toDateString())->where('professor_id', Auth::user()->professor_id)->count('created_at');
        $notices = Notice::whereBetween('koha_fillimit',  [ Carbon::parse('january')->startOfMonth(), Carbon::parse('june')->endOfMonth()])->where('role', '!=', 1)->get();
        $subject = ProfessorsSubject::where('professor_id', '=', Auth::user()->professor_id)->count('subject_id');

        return view('professors.dashboard', compact('notices', 'absences', 'marks','subject'));
    }
	public function marks(){

		$professors = ProfessorsSubject::with('subject')->select('subject_id')->where('professor_id', Auth::user()->professor_id)->get();

		$class = Clas::all();

		return view('professors.mark', compact('professors','class'));
	}

	public function testMarks(){

		$professors = ProfessorsSubject::with('subject')->select('subject_id')->where('professor_id', Auth::user()->professor_id)->get();

        $class = Clas::all();

		return view('professors.testmarks', compact('class','professors'));
	}


	public function ajaxStudent(){

		$clas_id = Input::get('clas_id');

		$students = Students::where('clas_id', $clas_id)->get();

		return \Response::json($students);
	}

    public function ajaxMarks(){

        $clas_id = Input::get('clas_id');

        $students = Students::where('clas_id', $clas_id)->get();

        return \Response::json($students);
    }

	public function storeMark(Request $request) {
        $this->validate($request, [

            'subject' => 'required',
            'class' => 'required',
            'student' => 'required',
            'nota' => 'required'

        ]);

        $marks = new Mark;

        $marks->student_id = $request->student;
        $marks->clas_id = $request->class;
        $marks->subject_id = $request->subject;
        $marks->mark = $request->nota;
        $marks->professor_id = Auth::user()->professor_id;
        $marks->save();
        return redirect('/professor/marks');
    }

    public function storeTestmarks(Request $request){
    	

        foreach($request->student as $key => $s) {

        	$data = array('student_id'=>$s, 
        				'clas_id'=>$request->class[$key],
        				'subject_id'=>$request->subject[$key],
        				'mark'=>$request->mark[$key],
        				'professor_id'=>Auth::user()->professor_id);

        	Mark::insert($data);
        }
        return redirect('/professor/testmarks');
    }

    public function professorMarks(){

        $marks = Mark::with('student','clas','subject')->where('professor_id', Auth::user()->professor_id)->get();
        
        return view('professors/classmark', compact('marks'));
    }

    public function getList($subject_id, $student_id){

        $marks = Mark::where('student_id', $student_id)
        ->where('subject_id', $subject_id)
        ->orderBy('id', 'desc')
        ->first();

        return view('professors/updateMarks', ['marks' => $marks]);
    }

    public function editMarks(){

        $professors = ProfessorsSubject::with('subject')->select('subject_id')->where('professor_id', Auth::user()->professor_id)->get();

        $class = Clas::all();

        return view('professors/editMarks', compact('professors','class'));
    }

    public function updateMarks(Request $request, $id){

        $this->validate($request, [
            'mark' => 'required'
        ]);
        $marks = Mark::find($id);
        $marks->mark = $request->mark;
        $marks->save();
        return redirect('professors/editMarks');

    }

    public function absence(){

        $professors = ProfessorsSubject::with('subject')->select('subject_id')->where('professor_id', Auth::user()->professor_id)->get();

        $class = Clas::all();

        return view('professors.absence', compact('class','professors'));
    }
    public function storeAbsence(Request $request) {

        foreach($request->student as $key => $s) {
            $arsye = '';
            if ($request->arsye[$key] == '') {
              $arsye =  0;
            }else{
                $arsye =  $request->arsye[$key];

            }
            $august = new Carbon('August');
            $december = new Carbon('December');
            $january = new Carbon('January');
            $july = new Carbon('July');
            $now = Carbon::now(); 

              
            if ($now->month > $august->month && $now->month < $january->month ) {
              $semestri =  1;
            }else{
                $semestri =  2;
            }

            $data = array('student_id'=>$s, 
                        'clas_id'=>$request->class[$key],
                        'subject_id'=>$request->subject[$key],
                        'professor_id'=>Auth::user()->professor_id,
                        'koha'=>$request->koha[$key],
                        'semester'=>$semestri);

            Absence::insert($data);
        }
        return redirect()->route('absence');
    }

    public function editAbsence(){

    $professors = ProfessorsClas::with('absence')->select('clas_id')->where('professor_id', Auth::user()->professor_id)->first();

    $absences = Absence::with('student','clas','subject')->where('clas_id', $professors->clas_id)->orderBy('koha', 'desc')->get();

        return view('professors/editabsence', compact('absences'));
    }

    public function professorAbsences(){

        $absences = Absence::with('student','clas','subject')->where('professor_id', Auth::user()->professor_id)->orderBy('koha', 'desc')->where('koha', '>=', \Carbon\Carbon::now()->subHour(1))->get();
        
        return view('professors/classabsence', compact('absences'));
    }

    public function getAbsence($id){

        $absences = Absence::find($id);

        return view('professors/updateAbsence', ['absences' => $absences]);
    }

    public function updateAbsence(Request $request, $id){

        $this->validate($request, [
            'arsye' => 'required'
        ]);
        $absence = Absence::find($id);
        $absence->arsye = $request->arsye;
        $absence->pershkrimi = $request->pershkrimi;
        $absence->update();
        return redirect()->route('editAbsence');
    }

    public function destroy($id){
    $absences = Absence::destroy($id);

        if ($absences == true) {

            return redirect()->back()->with('message', 'Mungesa u fshie me sukses!');
        }      
    }

    public function programi(){

        $professors = ProfessorsSubject::with('subject')->select('subject_id')->where('professor_id', Auth::user()->professor_id)->get();

        $class = Clas::all();

        return view('professors.ora', compact('class','professors'));
    }
    public function storeProgram(Request $request) {

         $this->validate($request, [
            'class' => 'required',
            'subject' => 'required',
            'ora' => 'required'
        ]);
            
            $august = new Carbon('August');
            $december = new Carbon('December');
            $january = new Carbon('January');
            $july = new Carbon('July');
            $now = Carbon::now(); 

            if ($now->month > $august->month && $now->month < $january->month ) {
              $semestri =  1;
            }else{
                $semestri =  2;
            }

        $programi = new Programi;

        $programi->subject_id = $request->subject;
        $programi->clas_id = $request->class;
        $programi->professor_id = Auth::user()->professor_id;
        $programi->ora = $request->ora;
        $programi->pershkrimi = $request->pershkrimi;
        $programi->semester = $semestri;
        $programi->save();

        return redirect()->route('oraMesimore');
    }
}
