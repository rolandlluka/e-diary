<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class ProfessorMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->role_id != '3')
        {
            return response()->view('errors.401');
        }

        return $next($request);
    }
}
