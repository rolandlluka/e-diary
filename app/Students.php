<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Students extends Model
{
    protected $fillable = ['name', 'lastname', 'clas_id'];


    public function parent(){

    	return $this->belongsTo('App\Parents');
    }
    public function academic(){

    	return $this->belongsTo('App\AcademicYear');
    }
    public function clas(){

    	return $this->belongsTo('App\Clas');
    }
    public function mark(){

    return $this->hasMany('App\Mark');
	}
    public function absence(){

    return $this->hasMany('App\Absence');
    }
}
