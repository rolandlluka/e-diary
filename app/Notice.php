<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notice extends Model
{
	protected $fillable = [
        'pershkrimi', 'role', 'koha_fillimit','koha_mbarimit'];

    public $timestamps = false;
}
