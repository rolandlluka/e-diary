<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Professor extends Model
{
    public function subjects(){

        return $this->belongsToMany('App\Subjects', 'professors_subjects', 'professor_id', 'subject_id', 'id');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'users_id', 'name');
    }
    public function schedule(){

        return $this->hasMany('App\Shcedule');
    }
    public function programi(){

        return $this->hasMany('App\Programi');
    }
}
