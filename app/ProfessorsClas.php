<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfessorsClas extends Model
{
	
    public function professor(){

        return $this->belongsTo('App\Professor');
    }
    
    public function clas(){

        return $this->belongsTo('App\Clas');
    }

    public function absence(){

        return $this->belongsTo('App\Absence');
    }
}
