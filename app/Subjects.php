<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subjects extends Model
{
    public function professor(){

        return $this->belongsToMany('App\Professor', 'professors_subjects', 'subject_id', 'professor_id', 'id');
    }
    public function user()
    {
        return $this->hasOne('App\User', 'users_id', 'name');
    }
    public function schedule(){

        return $this->hasMany('App\Shcedule');
    }
    public function mark(){

        return $this->hasMany('App\Mark');
    }

    public function absence(){

        return $this->hasMany('App\Absence');
    }

    public function programi(){

        return $this->hasMany('App\Programi');
    }
}
