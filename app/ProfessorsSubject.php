<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfessorsSubject extends Model
{
	
    public function professor(){

        return $this->belongsTo('App\Professor');
    }
    public function subject(){

        return $this->belongsTo('App\Subjects');
    }

	
}
