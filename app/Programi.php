<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Programi extends Model
{
	public $table = "programi";
	public $timestamps = false;

    public function clas(){

    	return $this->belongsTo('App\Clas');
    }
    public function subject(){

    	return $this->belongsTo('App\Subjects');
    }
    public function professor(){

    	return $this->belongsTo('App\Professor');
    }

}
