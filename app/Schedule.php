<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    public $timestamps = false;
    /*public function students(){

        return $this->belongsToMany('App\Students');
    }*/
    public function clas(){

        return $this->belongsTo('App\Clas');
    }
    public function subject(){

        return $this->belongsTo('App\Subjects');
    }
    public function professor(){

        return $this->belongsTo('App\Professor');
    }
}
